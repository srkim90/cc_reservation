import re
import sys
from datetime import datetime, timedelta

from ccsite.geojeviewcc.controller import GeojeviewccController
from chinjucc.controller import ChinjuccController
from common.order_model import OrderReservation


from define import *

from PyQt5.QtWidgets import QApplication

import dialog
from shadowBotChinjucc import shadowBotChinjucc
from shadowBotNobelcc import shadowBotNobelcc
from shadowBotGeojeviewcc import shadowBotGeojeviewcc


class MainWindowSite(dialog.MainWindow):

    def __init__(self):
        self.site_list = ["노벨CC", "진주CC", "거제CC"]
        self.app = QApplication(sys.argv)
        dialog.MainWindow.__init__(self)
        return

    def on_combobox_changed(self, value):
        form_name = "코스선택"
        form = self.combobox_dict[form_name]
        form.clear()
        for name in self.set_course(value):
            form.addItem(name)

    def set_course(self, site_name):
        if site_name == "노벨CC":
            return shadowBotNobelcc.get_course_list()
        elif site_name == "진주CC":
            return shadowBotChinjucc.get_course_list()
        elif site_name == "거제CC":
            return shadowBotGeojeviewcc.get_course_list()
        return []

    def __time_validate(self, date_text):
        try:
            datetime.strptime(date_text, '%Y-%m-%d')
        except ValueError:
            return False
        return True

    def check_validation(self):
        param_dict = self._build_form_params()
        if param_dict["QTextEdit"]["아이디"] == "":
            self._message_box("'아이디'에 사이트 ID를 입력하세요")
            return False
        if param_dict["QTextEdit"]["비밀번호"] == "":
            self._message_box("'비밀번호'에 사이트 Password를 입력하세요")
            return False
        input_date: str = param_dict["QTextEdit"]["날짜"].strip().replace(" ", "")
        if input_date == "":
            self._message_box("날짜에 예약할 일자를 입력하세요")
            return False
        input_date = input_date.split(",")
        for item in input_date:
            if self.__time_validate(item) is False:
                self._message_box("휴효하지 않은 날짜 형식")
                return False
        now = datetime.now()
        endtime = now + timedelta(days=90)
        nowDate = int(now.strftime('%Y%m%d'))
        endDate = int(endtime.strftime('%Y%m%d'))
        #input_date = int(input_date.replace("-", ""))
        #if nowDate >= input_date or endDate < input_date:
        #    self._message_box("익일부터 90일 이내의 날짜를 입력하세요")
        #    return False

        hope_time = self.check_time_validation(param_dict["QTextEdit"]["희망시간"])
        if hope_time == None:
            self._message_box("올바른 희망시간을 입력하세요 : eg. 09:35")
            return False
        return True

    def select_robot(self):
        input_param = self.get_param_dict()
        hope_time = input_param["QTextEdit"]["희망시간"]
        uid = input_param["QTextEdit"]["아이디"]
        pwd = input_param["QTextEdit"]["비밀번호"]
        day = input_param["QTextEdit"]["날짜"].replace("-", "")
        course_idx = input_param["QComboBox"]["코스선택"]
        site_idx = input_param["QComboBox"]["장소선택"]

        # print("ccsite=%s, course=%s, day=%s, pwd=%s, uid=%s, hope_time=%s" % (site_idx, course_idx, day, pwd, uid, hope_time))

        site_name = self.site_list[site_idx]
        if site_name == "노벨CC":
            user_name = None
            user_phone = None
            return shadowBotNobelcc(site_name, user_name, user_phone, day, hope_time, course_idx, uid, pwd)
        elif site_name == "거제CC":
            hope_time_hh = hope_time.split(":")[0]
            hope_time_mm = hope_time.split(":")[1]
            return GeojeviewccController(OrderReservation(
                uid,
                pwd,
                hope_time_hh,
                hope_time_mm,
                day
            ))

        elif site_name == "진주CC":
            user_name = None
            user_phone = None
            # return shadowBotChinjucc(site_name, user_name, user_phone, day, hope_time, course_idx, uid, pwd)
            hope_time_hh = hope_time.split(":")[0]
            hope_time_mm = hope_time.split(":")[1]
            return ChinjuccController(OrderReservation(
                uid,
                pwd,
                hope_time_hh,
                hope_time_mm,
                day
            ))
        else:
            print(" 자동예약 [%s] 은 아직 구현되지 않았습니다." % (self.site_list[site_idx]))
        return None

    def run_window(self):
        self.add_login_form()
        self.add_calendar()
        self.add_edit_line("희망시간")
        self.add_comdobox("장소선택", self.site_list).currentTextChanged.connect(self.on_combobox_changed)
        self.add_comdobox("코스선택", [])

        self.initUI(title="%s<%s> CC Automatic Reservation by %s" % (ROBOT_VERSION, UPDATE_DATE, AUTHOR,))
        self.add_summit_buttion()
        self.on_combobox_changed(self.site_list[0])
        self.show()
        self.app.exec_()
        try:
            e = self.select_robot()
        except Exception as e:
            print(e)
            return None
        return e
