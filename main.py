#!/bin/env python
import copy
import time
from datetime import datetime
from time import sleep

from PyQt5.QtCore import QThread

from ccsite.geojeviewcc.controller import GeojeviewccController
from chinjucc.controller import ChinjuccController
from nobelcc_dialog import MainWindowSite
from logging import *

from shadowBotBase_onlyNobelcc import shadowBotBaseNobelCc
from shadowBotNobelcc import shadowBotNobelcc

g_stop_flags = False


def on_success(e, course, day_list):
    yyyymmdd = course["yyyymmdd"]
    hhmm = course["hhmm"]
    course = e.get_course_mapping(course["course"])
    str_reservation_time = datetime.strptime(yyyymmdd + hhmm, "%Y%m%d%H%M%S").strftime(
        "%Y-%m-%d %H:%M:%S")
    e.log("예약 성공!! 예약 일시:%s, 코스명:%s" % (str_reservation_time, course), loglv=CRITICAL)
    if yyyymmdd in day_list:
        day_list.remove(yyyymmdd)


class Worker(QThread):
    def __init__(self, e):
        super().__init__()
        self.e = e

    def date_change(self, e):
        try:
            e.order.yyyymmdd = e.order.yyyymmdd.strip().replace(" ", "")
        except AttributeError as e:
            pass

    def check_date(self, yyyymmdd:str):
        if "," in yyyymmdd:
            yyyymmdd = yyyymmdd.split(",")[0]
        return yyyymmdd

    def run(self):
        e = self.e
        self.date_change(e)
        if e.__class__ == shadowBotNobelcc:
            #e.order.yyyymmdd = self.check_date(e.order.yyyymmdd)
            self.run_nobelcc()
        elif e.__class__ == ChinjuccController:
            e.order.yyyymmdd = self.check_date(e.order.yyyymmdd)
            self.run_chinjucc()
        elif e.__class__ == GeojeviewccController:
            self.run_geojeviewcc()
        else:
            self.run_general()
        return

    def run_chinjucc(self):
        self.e.run()

    def run_geojeviewcc(self):
        self.e.run()

    def run_general(self):
        e = self.e
        if e.do_login() is False:
            e.log("로그인 실패", loglv=CRITICAL)
            return
        e.get_user_info()

        if e.check_cancel():
            e.log("예약 취소 시도, 결과는 모름")
            e.log("프로그램을 종료 합니다.", loglv=CRITICAL)
            return
        e.init_auto_login()

        day_list = copy.deepcopy(e.yyyymmdd)

        while g_stop_flags is False:
            course_list = e.get_list(day_list)
            if course_list is None or len(course_list) == 0:
                e.sleep()
                continue
            e.log("try day_list : %s, len(course_list)=%d" % (day_list, len(course_list)))
            multi_result = e.do_book_multi(course_list)
            if multi_result is None:
                for idx, course in enumerate(course_list):
                    yyyymmdd = course["yyyymmdd"]
                    e.log("[%s/%s] Enter yyyymmdd=%s" % (idx, len(course_list), yyyymmdd, ))
                    if yyyymmdd in copy.deepcopy(day_list):
                        if e.do_book(course) is True:
                            on_success(e, course, day_list)
                        else:
                            e.log("예약 실패 | 재시도 : 해당 시간대의 예약을 시도하였으나 선점 당하였습니다.", loglv=CRITICAL)
                            e.log("일자: %s, 시간: %s, 코스: %s" % (course["yyyymmdd"], course["hhmm"], e.get_course_mapping(course["course"])))
                    else:
                        e.log("yyyymmdd = %s not in day_list" % (yyyymmdd,))
            else:
                if multi_result is not False:
                    course = multi_result
                    on_success(e, course, day_list)
            e.log("continue")
            if len(day_list) == 0:
                e.log("모든 예약을 완로 하였습니다.", loglv=CRITICAL)
                e.log("프로그램을 종료 합니다.", loglv=CRITICAL)
                break
        e.log("end-main")

    def run_nobelcc(self):
        e = self.e
        if e.do_login() is False:
            e.log("로그인 실패", loglv=CRITICAL)
            return
        #return
        e.get_user_info()

        if e.check_cancel():
            e.log("예약 취소 시도, 결과는 모름")
            e.log("프로그램을 종료 합니다.", loglv=CRITICAL)
            return
        e.init_auto_login()

        day_list = copy.deepcopy(e.yyyymmdd)

        while g_stop_flags is False:
            course_list = e.get_list(day_list)
            if len(course_list) == 0:
                e.sleep()
                continue
            e.log("try day_list : %s, len(course_list)=%d" % (day_list, len(course_list)))
            for idx, course in enumerate(course_list):
                yyyymmdd = course["yyyymmdd"]
                e.log("[%s/%s] Enter yyyymmdd=%s" % (idx, len(course_list), yyyymmdd, ))
                if yyyymmdd in copy.deepcopy(day_list):
                    if e.do_book(course) is True:
                        hhmm = course["hhmm"]
                        course = e.get_course_mapping(course["course"])
                        str_reservation_time = datetime.strptime(yyyymmdd+hhmm, "%Y%m%d%H%M%S").strftime("%Y-%m-%d %H:%M:%S")
                        e.log("예약 성공!! 예약 일시:%s, 코스명:%s" % (str_reservation_time, course), loglv=CRITICAL)
                        day_list.remove(yyyymmdd)
                    else:
                        e.log("예약 실패 | 재시도 : 해당 시간대의 예약을 시도하였으나 선점 당하였습니다.", loglv=CRITICAL)
                        e.log("일자: %s, 시간: %s, 코스: %s" % (course["yyyymmdd"], course["hhmm"], e.get_course_mapping(course["course"])))
                else:
                    e.log("yyyymmdd = %s not in day_list" % (yyyymmdd,))
            e.log("continue")
            if len(day_list) == 0:
                e.log("모든 예약을 완로 하였습니다.", loglv=CRITICAL)
                e.log("프로그램을 종료 합니다.", loglv=CRITICAL)
                break
        e.log("end-main")


def main():
    global g_stop_flags

    win = MainWindowSite()
    e = win.run_window()
    if e is None:
        print("자동 예약 프로그램 초기화 실패")
        return


    worker = Worker(e)
    worker.start()
    time.sleep(1.2)
    e.log_window_th(win.app)



    g_stop_flags = True
    sleep(1.8)
    return

if __name__ == '__main__':
    main()
