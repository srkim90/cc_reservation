import threading
from time import *
from datetime import datetime, timedelta
from logging import *
from shadowBotBase import shadowBotBase
from utils import *
import requests


class shadowBotGeojeviewcc(shadowBotBase):
    def __init__(self, site_name, yyyymmdd, prefer_time, prefer_course_idx, uid, passwd):
        site_addr = "https://www.geojeview.co.kr"
        user_name = None
        user_phone = None
        shadowBotBase.__init__(self, site_name, site_addr, user_name, user_phone, yyyymmdd, prefer_time, uid, passwd, prefer_course_idx)
        self.auth_login_ranges = None
        self.login_timeout_sec = 50
        if self.weekday == "토":
            next_yyyymmdd = (datetime.strptime("%s 12:00:00" % yyyymmdd, "%Y%m%d %H:%M:%S") + timedelta(days=1)).strftime("%Y%m%d")
            self.yyyymmdd.append(next_yyyymmdd)
        elif self.weekday == "일":
            next_yyyymmdd = (datetime.strptime("%s 12:00:00" % yyyymmdd, "%Y%m%d %H:%M:%S") - timedelta(days=1)).strftime("%Y%m%d")
            self.yyyymmdd.append(next_yyyymmdd)

    @staticmethod
    def get_course_list():
        return ["임의의코스", "해돋이", "해넘이"]

    def get_course_mapping_AB(self, course_info):
        course_dict = {
            "해돋이": 'A',
            "해넘이": 'B',
        }
        try:
            return course_dict[course_info]
        except KeyError:
            return None

    def get_course_mapping(self, course_info):
        if course_info == None:
            return "임의의코스"
        course_dict = {
            "임의의코스": 0,
            "해돋이": 1,
            "해넘이": 2,
        }
        # 이름으로 들어올 경우 숫자값 반환
        if course_info in course_dict.keys():
            return course_dict[course_info]

        # 숫자값으로 들어올 경우 이름 반환
        for course_name in course_dict.keys():
            if course_dict[course_name] == course_info:
                return course_name
        return None

    def __parse_course(self, json_list:list, yyyymmdd):
        result_list = []
        # [
        #     {
        #         "bk_cours": "A",
        #         "bk_part": "3",
        #         "bk_time": "1851",
        #         "bk_hole": "18",
        #         "bk_base_greenfee": "-",
        #         "bk_green_fee": "100,000",
        #         "bk_event": "",
        #         "bk_green_sale_rate": "-",
        #         "bk_cours_name": "해돋이"
        #     },
        # ...
        #]
        #print(json.dumps(html, indent=4, ensure_ascii=False))

        for item in json_list:
            hhmm = item["bk_time"]
            course = item["bk_cours_name"]
            form_data = {
                "yyyymmdd": yyyymmdd,
                "hhmm": hhmm,
                "course": course,
                "bk_cours" : item["bk_cours"],
                "bk_hole" : item["bk_hole"],
            }
            result_list.append(form_data)
        return result_list

    def get_list(self):
        selected_list = []
        for yyyymmdd in self.yyyymmdd:
            list_url = "booking/time/%s" % yyyymmdd

            result = self._get_with_cookie(list_url, None)
            if result.status_code != 200:
                self.log("fail to request : result.status_code=%s, url=%s" % (result.status_code,list_url))
                continue
            try:
                result = json.loads(result.text)
            except Exception as e:
                self.log("fail convert json : result=%s, error=%s, url=%s" % (result.text, e, list_url))
                continue
            items = self.__parse_course(result, yyyymmdd)
            if len(items) == 0:
                stat_report = self.add_try_history()
                if stat_report is not None:
                    self.log(stat_report)
                return selected_list
            selected_list.append(self.select_time(items, yyyymmdd))
            #print("!!!! %s" % items)
            selected_list += items
        return selected_list

    def do_cancel(self, yyyymmdd, hhmm, int_course):
        url = "html/reservation/reservation_02_01_ok.asp"
        form_data = {
            "book_date" : yyyymmdd,
            "book_crs" : int_course,
            "book_crs_name" : "",
            "book_time" : hhmm,
            "rec_name" : "",
        }
        self._post_text_with_cookie(url, form_data)
        return True

    def do_book(self, selected_dict: dict):
        if selected_dict is None:
            return False
        response_text = self._get_text_with_cookie("booking")
        _token = response_text.split('<meta name="csrf-token" content="')[1].split('"')[0]
        form_data = {
            "_token": _token,
            "date": selected_dict["yyyymmdd"], # 20210709
            "cours": selected_dict["bk_cours"], # B
            "time": selected_dict["hhmm"], #1912,
            "hole": selected_dict["bk_hole"],
            "incnt": 4,
            "booking_agree": 0,
        }
        form_data = self._dict_url_encode(form_data) + "&booking_agree=1"
        #printDict(form_data)

        url = "booking"
        result = self._post_with_cookie(url, form_data)
        self.display_cookies()

        sleep(999)
        # for keyname in result_dict.keys():
        if 'reservation_02_01' in result:
            return True
        return False

    def _parser_login_cookie(self, str_cookie):
        check_item = ["XSRF-TOKEN",
                      "app_session",
                      "secure, app_session",
                      "_gat_gtag_UA_56261698_2",
                      "_ga",
                      "_gid"]
        result_cookie = ""
        for item in str_cookie.split(";"):
            item = item.strip()
            if '=' not in item:
                continue
            tag = item.split("=")[0].split(" ")[-1]
            val = item.split("=")[1]
            if tag not in check_item:
                continue
            if len(result_cookie) != 0:
                result_cookie += "; "
            result_cookie += "%s=%s" % (tag, val)
        return result_cookie

    def make_login_data(self):
        self.end_session()
        self.start_session()
        form_url = self._make_url("auth/login")
        response = self.session.get(form_url)
        response.encoding = "UTF-8"
        _token = response.text.split('<input type="hidden" name="_token" value="')[1].split('"')[0]
        login_url = "auth/login"
        login_data = {
            "_token" : _token,
            "username": self.uid,
            "password": self.passwd,
            "remember": 1,
        }
        return self._dict_url_encode(login_data, order=["_token","username","password","remember"]), login_url, None#self._parser_login_cookie(response.headers["Set-Cookie"])

    def do_login(self):

        login_data, login_url, cookie = self.make_login_data()
        result = self._login(login_data,
                             login_url,
                             {"Cookie" : cookie})

        if result is not None and "redirect" in result:
            self.log("로그인 시도 성공 : id=%s" % (self.uid,))
            return True
        self.login_cookie = None
        self.log("로그인 시도 실패 : id=%s" % (self.uid,))
        return False

    def logout(self):
        return self._logout()

    def _logout(self):
        form_url = "auth/logout"
        response = self._get_text_with_cookie(form_url)
        super()._logout()
        return response

    def make_cookie_fn(self):
        login_data, login_url, cookie = self.make_login_data()
        return {"Cookie" : cookie}

    def init_auto_login(self):
        login_data, login_url, cookie = self.make_login_data()
        self.start_auto_login(login_data, login_url, self.make_cookie_fn)
