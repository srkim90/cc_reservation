import concurrent.futures
import threading
import time


def do_book_foo(course):
    time.sleep(3)
    return course + "CCCC"

def main():
    # course = "aaaa"
    # hThread = threading.Thread(target=do_book_foo, args=(course,))
    # hThread.daemon = True
    # hThread.start()
    # value = hThread.join()
    # print(value)
    #with concurrent.futures.ThreadPoolExecutor() as executor:
    list_thread = []
    for idx in range(12):
        executor = concurrent.futures.ThreadPoolExecutor()
        future = executor.submit(do_book_foo, ":%d" % idx)
        list_thread.append(future)
    for future in list_thread:
        return_value = future.result()
        print(return_value)


if __name__ == '__main__':
    main()
