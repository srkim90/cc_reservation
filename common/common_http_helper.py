import urllib

class CommonHttpHelper:
    def __init__(self) -> None:
        super().__init__()

    @staticmethod
    def dict_url_encode(data: dict) -> str:
        return urllib.parse.urlencode(data)
