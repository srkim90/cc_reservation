from typing import List


class OrderReservation:
    uid: str
    passwd: str
    prefer_time_hh: str
    prefer_time_mm: str
    yyyymmdd : str

    def __init__(self, uid: str, passwd: str, prefer_time_hh: str, prefer_time_mm: str, yyyymmdd: str) -> None:
        super().__init__()
        self.uid = uid
        self.passwd = passwd
        self.yyyymmdd = yyyymmdd
        self.prefer_time_hh = prefer_time_hh
        self.prefer_time_mm = prefer_time_mm

    def get_prefer_time(self):
        return "%s:%s" % (self.prefer_time_hh, self.prefer_time_mm)