import copy
import gzip
import inspect
import json
import os
import shutil
from datetime import datetime

from common.cc_loglevel import DEBUG
from dialog import logWindow


log_window: logWindow = None


class Logging:
    def __init__(self) -> None:
        super().__init__()

    @staticmethod
    def set_log_window(new_log_window: logWindow):
        global log_window
        log_window = new_log_window

    @staticmethod
    def printDict(dictAt, cout=True):
        if type(dictAt) != dict:
            print("%s" % dictAt)
            return None
        try:
            jsonAt = json.dumps(dictAt, indent=4)
        except TypeError as e:
            try:
                dictAt = Logging.time_to_string_dict(dictAt)
            except Exception:
                print("%s" % dictAt)
                return None
            try:
                jsonAt = json.dumps(dictAt, indent=4)
            except TypeError:
                print("%s" % dictAt)
                return None

        if cout is True:
            print(jsonAt)
        return jsonAt

    @staticmethod
    def time_to_string_dict(dictAt):
        dictAt = copy.deepcopy(dictAt)
        for key_name in dictAt.keys():
            if type(dictAt[key_name]) == dict:
                dictAt[key_name] = Logging.time_to_string_dict(dictAt[key_name])
            elif type(dictAt[key_name]) == list:
                for idx, item in enumerate(dictAt[key_name]):
                    # print(type(dictAt[key_name][idx]))
                    if type(dictAt[key_name][idx]) == datetime:
                        # print("SAAA")
                        dictAt[key_name][idx] = dictAt[key_name][idx].strftime("%Y-%m-%d %H:%M:%S")
            elif type(dictAt[key_name]) == datetime:
                dictAt[key_name] = dictAt[key_name].strftime("%Y-%m-%d %H:%M:%S")
        return dictAt

    @staticmethod
    def log(log_text, loglv=DEBUG):
        # log_text = "%s" % log_text
        if log_window is None:
            print(log_text)
        else:
            log_window.log_fn(log_text, loglv)
        log_name = "window_log_" + datetime.now().strftime("%Y%m%d") + ".log.gz"
        Logging.log_as_file(log_name, log_text)

    @staticmethod
    def log_as_file(log_text, tag="root"):
        log_name = datetime.now().strftime("%Y%m%d") + ".log.gz"
        FILE_SIZE = 1024 * 1024 * 256
        LOG_DIR = "E:\\log\\bot"
        if os.path.exists(LOG_DIR) is False:
            return

        log_path = os.path.join(LOG_DIR, log_name)
        if os.path.exists(log_path) is True:  # 파일 크기가 큰 경우 옮김
            if os.path.getsize(log_path) > FILE_SIZE:
                new_file = None
                for idx in range(100):
                    _new_file = log_path.replace(".gz", "")
                    _new_file = _new_file + ".%02d.gz" % (idx,)
                    if os.path.exists(_new_file) is False:
                        new_file = _new_file
                        break
                if new_file is None:
                    return
                shutil.move(log_path, new_file)
        if type(log_text) == dict:
            log_text = json.dumps(log_text)
        elif type(log_text) == list:
            log_text = "%s" % (log_text,)
        else:
            log_text = "%s" % (log_text,)
        if type(log_text) == str:
            log_text = log_text.encode('utf-8')
        func_info = Logging.func(n_caller_stack=2).encode("utf-8")
        with gzip.open(log_path, "ab") as fd:
            now_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S").encode('utf-8')
            fd.write(b"= [%s][%s][%s] ========================================================\n" % (
                now_time, func_info, tag.encode("utf-8")))
            fd.write(b"%s" % log_text)
            fd.write(b"\n=========================================================\n")
        return

    @staticmethod
    def func(n_caller_stack=1):
        caller = inspect.stack()[n_caller_stack]
        return "%s(%d):%s" % (caller.filename, caller.lineno, caller.function)
