import gzip
import json
import os
import shutil
import threading
import time
from abc import abstractmethod
from datetime import datetime, timedelta
from http import HTTPStatus
from operator import itemgetter
from time import *
from logging import *
import urllib

from dialog import logWindow
import requests
from define import *
from utils import func, printDict


class shadowBotBase:
    last_login: datetime

    def __init__(self, site_name, site_addr, user_name, user_phone, yyyymmdd, prefer_time, uid, passwd,
                 prefer_course_idx=None):
        self.site_name = site_name
        self.site_addr = site_addr
        self.login_cookie = None
        self.login_time = None
        self.user_name = user_name
        self.user_phone = user_phone
        self.prefer_course_idx = prefer_course_idx
        self.prefer_time = prefer_time
        self.yyyymmdd = [yyyymmdd, ]
        self.login_timeout_sec = None
        self.uid = uid
        self.passwd = passwd
        self.auth_login_ranges = None  # [["08:40", "09:20"], ["14:40", "15:20"]]
        self.log_window = None
        self.ln_try = 0
        self.last_report = None
        self.sleep_time = 1.0
        self.report_delay = 300
        self.weekday = self.get_weekday(yyyymmdd)
        self.session = None
        self.last_login = None
        self.already_booked = []

    def start_session(self):
        self.session = requests.sessions.Session()

    def end_session(self):
        if self.session is not None:
            self.session.close()
            self.session = None

    def get_user_info(self):
        pass

    def check_cancel(self):
        return False

    def get_weekday(self, yyyymmdd):
        week_name = ['월', '화', '수', '목', '금', '토', '일']
        weekday = datetime.strptime("%s 12:00:00" % yyyymmdd, "%Y%m%d %H:%M:%S").weekday()
        return week_name[weekday]

    def add_try_history(self):
        self.ln_try += 1
        f_report = False
        if self.last_report == None:
            f_report = True
        else:
            if datetime.now().timestamp() - self.last_report.timestamp() > self.report_delay:
                f_report = True
        if f_report == False:
            return None

        from_hhmmss = datetime.now().strftime("%H:%M:%S")
        if self.last_report is not None:
            from_hhmmss = self.last_report.strftime("%H:%M:%S")
        end_hhmmss = datetime.now().strftime("%H:%M:%S")
        log_string = "%s ~ %s 동안 %d 회 예약 가능 여부 확인 시도 하였습니다." % (from_hhmmss, end_hhmmss, self.ln_try)
        if self.last_report == None:
            log_string = [log_string, ]
            log_string.append("이후 %s초마다 예약 시도하며, %s초마다 보고 합니다." % (self.sleep_time, self.report_delay))

        self.last_report = datetime.now()
        self.ln_try = 0
        return log_string

    def print_init_log(self):
        self.log("%s<%s> 자동 예약 프로그램 기동 성공" % (self.site_name, self.site_addr))
        self.log("version=%s date=%s, author=%s" % (ROBOT_VERSION, UPDATE_DATE, AUTHOR))
        self.log("  -- 사이트 이름 : %s <%s>" % (self.site_name, self.site_addr))
        self.log("  -- 가입자 계정 : %s" % (self.uid,))
        self.log("  -- 가입자 비밀번호 : %s" % ("*" * len(self.passwd),))
        for yyyymmdd in self.yyyymmdd:
            self.log("  -- 예약 일자 : %s (%s)" % (yyyymmdd, self.get_weekday(yyyymmdd)))
        self.log("  -- 예약 시간 : %s" % (self.prefer_time,))
        course_mapping = self.get_course_mapping(self.prefer_course_idx)
        if hasattr(self, "no_caddy"):
            if self.no_caddy is True:
                course_mapping += '(노캐디)'
        self.log("  -- 예약 코스 : %s" % (course_mapping,))

    def log_window_th(self, app):
        self.log_window = logWindow()
        self.log_window.resize(640, 480)
        self.log_window.show()
        self.print_init_log()
        app.exec_()

    def log(self, log_text, loglv=DEBUG):
        # log_text = "%s" % log_text
        if self.log_window == None:
            print(log_text)
        else:
            self.log_window.log_fn(log_text, loglv)
        log_name = "window_log_" + datetime.now().strftime("%Y%m%d") + ".log.gz"
        self._log_as_file(log_name, log_text, "root")

    def log_as_file(self, log_text, tag="root"):
        log_name = datetime.now().strftime("%Y%m%d") + ".log.gz"
        self._log_as_file(log_name, log_text, tag)

    def _log_as_file(self, log_name, log_text, tag):
        FILE_SIZE = 1024 * 1024 * 256
        LOG_DIR = "E:\\log\\bot"
        if os.path.exists(LOG_DIR) is False:
            return

        log_path = os.path.join(LOG_DIR, log_name)
        if os.path.exists(log_path) is True:  # 파일 크기가 큰 경우 옮김
            if os.path.getsize(log_path) > FILE_SIZE:
                new_file = None
                for idx in range(100):
                    _new_file = log_path.replace(".gz", "")
                    _new_file = _new_file + ".%02d.gz" % (idx,)
                    if os.path.exists(_new_file) is False:
                        new_file = _new_file
                        break
                if new_file is None:
                    return
                shutil.move(log_path, new_file)
        if type(log_text) == dict:
            log_text = printDict(log_text)
        elif type(log_text) == list:
            log_text = "%s" % (log_text,)
        else:
            log_text = "%s" % (log_text,)
        if type(log_text) == str:
            log_text = log_text.encode('utf-8')
        func_info = func(n_caller_stack=2).encode("utf-8")
        with gzip.open(log_path, "ab") as fd:
            now_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S").encode('utf-8')
            fd.write(b"= [%s][%s][%s] ========================================================\n" % (
                now_time, func_info, tag.encode("utf-8")))
            fd.write(b"%s" % log_text)
            fd.write(b"\n=========================================================\n")
        return

    def sleep(self):
        sleep(self.sleep_time)

    def _make_url(self, login_url):
        return "%s/%s" % (self.site_addr, login_url)

    def _dict_url_encode(self, data, order=None):
        # result = ""
        # if order == None:
        #     order = data.keys()
        # for idx,key in enumerate(order):
        #     if idx != 0:
        #         result += "&"
        #     result += "%s=%s" % (key, data[key])
        # result = "\r\n" + result

        return urllib.parse.urlencode(data)

    def html_header_to_json(self, html_head):
        new_dict = {}
        for item in html_head.keys():
            new_dict[item] = "%s" % html_head[item]
        return json.dumps(new_dict, indent=4)

    def add_cookies(self, cookie_list):
        result_cookie = ""
        ignore = ["expires", "max-age", "samesite", ]  # "path",
        already_exist = []
        for str_cookie in cookie_list:
            if str_cookie == None:
                continue
            for item in str_cookie.split(";"):
                item = item.strip()
                if '=' not in item:
                    continue
                tag = item.split("=")[0].split(" ")[-1]
                val = item.split("=")[1]

                if tag.lower() in ignore:
                    continue
                if tag in already_exist:
                    continue
                if len(result_cookie) != 0:
                    result_cookie += "; "
                result_cookie += "%s=%s" % (tag, val)
                already_exist.append(tag)
        return result_cookie

    def display_cookies(self):
        login_cookie: str = self.login_cookie
        login_cookie = login_cookie.split(";")
        print("--------------------------")
        for cookie in login_cookie:
            cookie = cookie.strip()
            key = cookie.split("=")[0]
            value = cookie.split("=")[-1]
            print("key : %s, value : %s" % (key, value))
        print("--------------------------")

    def _login(self, login_data, login_url, add_headers=None):
        login_url = self._make_url(login_url)

        headers = self._make_http_heades()
        # if type(login_data) == str:
        #     headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
        #     headers['Host'] = "www.geojeview.co.kr"
        #     headers['Origin'] = "https://www.geojeview.co.kr"
        #     headers['Referer'] = "https://www.geojeview.co.kr/auth/login"
        #     headers['sec-ch-ua'] = 'Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"'
        #     headers['sec-ch-ua-mobile'] = "?0"
        #     headers['Sec-Fetch-Dest']  = "empty"
        #     headers['Sec-Fetch-Mode'] = "cors"
        #     headers['Sec-Fetch-Site'] = "same-origin"
        #     headers['X-Requested-With'] = "XMLHttpRequest"

        if add_headers is not None:
            for key_name in add_headers.keys():
                headers[key_name] = add_headers[key_name]

        if self.session is not None:
            if "Cookie" in headers.keys():
                del headers["Cookie"]
            res = self.session.post(login_url, data=login_data, headers=headers)
        else:
            res = requests.post(login_url, data=login_data, headers=headers)
        res.encoding = "UTF-8"

        # print("response : %s\n%s" % (res.status_code, self.html_header_to_json(res.headers)))
        # print("request  : %s" % self.html_header_to_json(res.request.headers))
        # print(res.text)
        # sleep(999)
        if res.status_code == HTTPStatus.OK or res.status_code == HTTPStatus.ACCEPTED:
            if "Set-Cookie" not in res.headers.keys():
                return None
            self.login_cookie = res.headers["Set-Cookie"]  # self.add_cookies([res.headers["Set-Cookie"], org_cookie])
            self.login_time = datetime.now()
            # self.log(self.login_cookie)
            # print(self.login_cookie)
            # sleep(9999)

            result = res.text  # .encode("utf-8").decode("utf-8")
            return result
        else:
            return None

    def _logout(self):
        self.login_cookie = None

    def need_login(self):
        if self.login_time is None:
            return True
        time_gap = datetime.now().timestamp() - self.login_time.timestamp()
        if time_gap > self.login_timeout_sec:
            return True
        return False

    def start_auto_login(self, login_data, login_url, make_cookie_fn=None):
        hThread = threading.Thread(target=self._login_thread, args=(login_data, login_url, make_cookie_fn))
        hThread.daemon = True
        hThread.start()

    @abstractmethod
    def check_login_success(self, html):
        pass

    def _login_thread(self, login_data, login_url, make_cookie_fn):
        sleep(2)
        cookie = None
        while True:
            if self.auth_login_ranges is not None:
                f_skip = True
                for range_pair in self.auth_login_ranges:
                    end_time = None
                    str_start_time = range_pair[0]
                    str_end_time = range_pair[1]
                    today_yyyymmdd = "%d%02d%02d" % (datetime.now().year, datetime.now().month, datetime.now().day)
                    if str_end_time is not None:
                        end_time = datetime.strptime("%s%s00" % (today_yyyymmdd, str_end_time.replace(":", "")),
                                                     "%Y%m%d%H%M%S")
                    start_time = datetime.strptime("%s%s00" % (today_yyyymmdd, str_start_time.replace(":", "")),
                                                   "%Y%m%d%H%M%S")

                    now_time = datetime.now()
                    # print("start_time : %s" % start_time)
                    # print("end_time : %s" % end_time)
                    # print("now_time : %s" % now_time)
                    if end_time is not None:
                        if start_time < now_time < end_time:
                            f_skip = False
                        break
                    else:
                        if start_time < now_time:
                            f_skip = False
                        break
            else:
                f_skip = False
            if self.need_login() and f_skip is False:
                self.log("자동 로그인 동작 : id=%s, 동작시간 : %s" % (self.uid, self.auth_login_ranges))
                self._logout()
                if self.do_login() is False:
                    if make_cookie_fn is not None:
                        cookie = make_cookie_fn()
                    result = self._login(login_data, login_url, cookie)
                    if self.check_login_success(result) is True:
                        self.log("자동 로그인 동작 성공 : id=%s" % (self.uid,))
                    else:
                        self.log("자동 로그인 동작, 로그인 실패!!! : id=%s, login_data=%s, login_url=%s" % (
                            self.uid, login_data, login_url))
                else:
                    self.log("자동 로그인 동작 성공 by do_login : id=%s" % (self.uid,))
                sleep(60.0)
            else:
                sleep(1.0)

    def do_login(self):
        return False

    def _make_http_heades(self):
        headers = {
            'User-Agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36",
            'Accept-Language': "ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6,zh;q=0.5",
        }
        return headers

    def _post_with_cookie(self, url, data, heads=None):
        url = self._make_url(url)

        # cookies = {'Cookie': self.login_cookie}
        headers = heads
        if heads is None:
            headers = self._make_http_heades()

        # cookies['Cookie'] = self.login_cookie
        if self.session is not None:
            result = self.session.post(url, data=data, headers=headers)
        else:
            cookies = None
            headers["Cookie"] = self.login_cookie
            # cookies = {'Cookie': self.login_cookie}
            result = requests.post(url, data=data, headers=headers, cookies=cookies)
        result.encoding = 'utf-8'

        # print(url)
        # print("response : %s\n%s" % (result.status_code, self.html_header_to_json(result.headers)))
        # print("request  : %s" % self.html_header_to_json(result.request.headers))
        # print(data)
        # print(result.text)

        return result

    def _post_text_with_cookie(self, url, data, heads=None):
        result = self._post_with_cookie(url, data, heads=heads)
        return result.text

    def _get_with_cookie(self, url, params=None, heads=None):
        url = self._make_url(url)
        # cookies = {'Cookie': self.login_cookie}
        if heads != None:
            headers = heads
        else:
            headers = self._make_http_heades()
        # cookies['Cookie'] = self.login_cookie
        if self.session is not None:
            result = self.session.get(url, params=params, headers=headers)
        else:
            # cookies = {'Cookie': self.login_cookie}
            # cookies = {'Cookie': 'MemberAgree=LTRwhg1qiCU=; LoginCheck=; LoginGubun=lqKd4xsWFuY=; MemberGNo=VkCPKvIppF2Wop3jGxYW5g==; MemberName=kFrFcoIA4wl2/b/sVjauGA==; MemberMobile=RjI7CAmhCmLc2eOIAQIUMw==; MemberID=MNkNDoQdTRo=; MemberNo=VkCPKvIppF2Wop3jGxYW5g==; MemberGubun=xkzxPqNDuE8=; MemberGroup=YI/UnGa0lEE=; MemberDepo=+H8eUfrFMOY=; MemberEmail=XLy8WdnwoBi4BwRdKkNFZIs5+n9/nrnb; MemberBoard=Z24KAnfClkY=; MemberCode=lFSE36lQ/u9osI/q2yAErw==; LastCheckTime=k3BTH36ctVE72RCRFsJ/fusjYlxCGZKXDowz6nDjNxA=; AWSALB=WiBIkJMHYXJvC6XFE+rMKJVFxuDkY6O9JPEcwtUnnH1nMmxMiXQBo/ChXPrMqq/5FA0SeZUL8J80GCPQxGYzZkQUAMJE1qKBAR1uwg9Xmz+i5jWjt0gA2JPmcq4v'}
            cookies = None
            headers[
                "Cookie"] = self.login_cookie  # 'MemberAgree=LTRwhg1qiCU=; LoginCheck=; LoginGubun=lqKd4xsWFuY=; MemberGNo=VkCPKvIppF2Wop3jGxYW5g==; MemberName=kFrFcoIA4wl2/b/sVjauGA==; MemberMobile=RjI7CAmhCmLc2eOIAQIUMw==; MemberID=MNkNDoQdTRo=; MemberNo=VkCPKvIppF2Wop3jGxYW5g==; MemberGubun=xkzxPqNDuE8=; MemberGroup=YI/UnGa0lEE=; MemberDepo=+H8eUfrFMOY=; MemberEmail=XLy8WdnwoBi4BwRdKkNFZIs5+n9/nrnb; MemberBoard=Z24KAnfClkY=; MemberCode=lFSE36lQ/u9osI/q2yAErw==; LastCheckTime=k3BTH36ctVE72RCRFsJ/fusjYlxCGZKXDowz6nDjNxA=; AWSALB=WiBIkJMHYXJvC6XFE+rMKJVFxuDkY6O9JPEcwtUnnH1nMmxMiXQBo/ChXPrMqq/5FA0SeZUL8J80GCPQxGYzZkQUAMJE1qKBAR1uwg9Xmz+i5jWjt0gA2JPmcq4v'
            result = requests.get(url, params=params, headers=headers, cookies=cookies)
        result.encoding = 'utf-8'

        # print(url)
        # print("response : %s\n%s" % (result.status_code, self.html_header_to_json(result.headers)))
        # print("request  : %s" % self.html_header_to_json(result.request.headers))

        return result

    def _get_text_with_cookie(self, url, params=None, heads=None):
        result = self._get_with_cookie(url, params=params, heads=heads)
        return result.text

    def get_course_mapping(self, course_info):
        return ""

    def get_login_time_after(self) -> int:
        if self.last_login is None:
            return -1
        return int(datetime.now().timestamp() - self.last_login.timestamp())

    def select_time(self, list_data, yyyymmdd, n_result=1):
        if len(list_data) == 0:
            return None
        prefer_date = datetime.strptime("%s%s00" % (yyyymmdd, self.prefer_time.replace(":", "")),
                                        "%Y%m%d%H%M%S").timestamp()
        time_gap = 3600 * 128  # 의미없는 큰 시간
        select_at = None
        self.log("=========================================")
        self.log("예약 가능한 시간대 리스트")
        self.log("-----------------------------------------")
        all_list = []
        for idx, item in enumerate(list_data):
            add_info = ""
            if "no_caddy" in item.keys():
                add_info = "노캐디" if item["no_caddy"] is True else "캐디"
            self.log("  - %s %s %s(%s) %s" % (
            item["yyyymmdd"], item["hhmm"], self.get_course_mapping(item["course"]), item["course"], add_info))
            if self.check_already_booked_time(item["yyyymmdd"], item["hhmm"], item["course"]) is True:
                self.log("SKIP")
                continue
            date_at = datetime.strptime("%s%s00" % (item["yyyymmdd"], item["hhmm"]), "%Y%m%d%H%M%S").timestamp()
            time_gap_at = abs(prefer_date - date_at)
            if time_gap > time_gap_at:
                time_gap = time_gap_at
                select_at = item
            all_list.append({
                "time_gap": time_gap_at,
                "item": item
            })

        self.log("-----------------------------------------")
        self.log("선택 된 시간")
        if n_result == 1:
            self.log("  - %s %s %s" % (
                select_at["yyyymmdd"], select_at["hhmm"], self.get_course_mapping(select_at["course"])))
            # return select_at
        else:
            _all_list = sorted(all_list, key=itemgetter('time_gap'), reverse=False)
            all_list = []
            for item in _all_list:
                all_list.append(item["item"])
            if len(all_list) >= n_result:
                select_at = all_list[0:n_result - 1]
            else:
                select_at = all_list
            for item in select_at:
                self.log("  - %s %s %s" % (item["yyyymmdd"], item["hhmm"], self.get_course_mapping(item["course"])))
        self.log("=========================================")
        return select_at

    def do_book_multi(self, course_list: list):
        return None

    def add_already_booked_time(self, yyyymmdd: str, hhmm: str, course: str):
        # 사이트에서 이미 부킹 되었다고 알려줄 경우, 해당 시간대는 다시 리스트업 하지 않는다.
        # "yyyymmdd": "2021-10-19"
        # "hhmm": "1154"
        # "course": "11"
        yyyymmdd = yyyymmdd.replace("-", "")
        yyyymmdd = yyyymmdd.replace(".", "")
        hhmm = hhmm.replace("-", "")
        hhmm = hhmm.replace(".", "")
        str_keyword = "%s:%s:%s" % (yyyymmdd, hhmm, course)
        if str_keyword not in self.already_booked:
            self.already_booked.append(str_keyword)

    def check_already_booked_time(self, yyyymmdd: str, hhmm: str, course: str) -> bool:
        str_keyword = "%s:%s:%s" % (yyyymmdd, hhmm, course)
        if str_keyword not in self.already_booked:
            return False
        return True
