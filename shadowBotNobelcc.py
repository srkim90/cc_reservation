import threading
from datetime import datetime, timedelta
from http import HTTPStatus
from time import *
from logging import *

from dialog import logWindow
from shadowBotBase_onlyNobelcc import shadowBotBaseNobelCc
from utils import *
import requests
from define import *





class shadowBotNobelcc(shadowBotBaseNobelCc):
    def __init__(self, site_name, user_name, user_phone, yyyymmdd, prefer_time, prefer_course_idx, uid, passwd):
        self.ln_print = 0
        site_addr = "https://www.nobelcc.co.kr"
        #prefer_course_idx = self.get_course_mapping(prefer_course_idx)
        shadowBotBaseNobelCc.__init__(self, site_name, site_addr, user_name, user_phone, yyyymmdd, prefer_time, uid, passwd, prefer_course_idx)
        self.auth_login_ranges = [
                                  ["09:55", "10:06"],
                                  ["13:55", "14:06"],
                                  ["00:00", "00:05"],
                                  ["23:55", "23:59"]]
        self.login_timeout_sec = 500
        # if self.weekday == "토":
        #     next_yyyymmdd = (datetime.strptime("%s 12:00:00" % yyyymmdd, "%Y%m%d %H:%M:%S") + timedelta(days=1)).strftime("%Y%m%d")
        #     self.yyyymmdd.append(next_yyyymmdd)
        # elif self.weekday == "일":
        #     next_yyyymmdd = (datetime.strptime("%s 12:00:00" % yyyymmdd, "%Y%m%d %H:%M:%S") - timedelta(days=1)).strftime("%Y%m%d")
        #     self.yyyymmdd.append(next_yyyymmdd)

    @staticmethod
    def get_course_list():
        return ["임의의코스", "가야", "충무", "공룡", "예약취소_가야", "예약취소_충무", "예약취소_공룡"]

    def check_cancel(self):
        if self.prefer_course_idx == None:
            return False
        #print("%s self.prefer_course_idx" % self.prefer_course_idx)
        last_idx = self.get_course_mapping("공룡")
        if self.prefer_course_idx > last_idx:
            self.do_cancel(self.yyyymmdd[0], self.prefer_time.replace(":",""), self.prefer_course_idx - last_idx) # 가야=1, 충무=2, 공룡=3
            return True
        return False

    def get_course_mapping(self, course_info):
        if course_info == None:
            return "임의의코스"
        course_dict = {
            "임의의코스": 0,
            "가야": 1,
            "충무": 2,
            "공룡": 3,
            "예약취소_가야" : 4,
            "예약취소_충무" : 5,
            "예약취소_공룡" : 6,
        }
        # 이름으로 들어올 경우 숫자값 반환
        if course_info in course_dict.keys():
            return course_dict[course_info]

        # 숫자값으로 들어올 경우 이름 반환
        for course_name in course_dict.keys():
            if course_dict[course_name] == course_info:
                return course_name
        return None

    def __parse_course(self, html: str):
        result_list = []
        for line in html.split("\n"):
            if 'JavaScript:Book_Confirm' not in line:
                continue
            # <td><a href="JavaScript:Book_Confirm('20210615','화','1', '가야','1838');"><span class="reservation_button_2">예약</span></a></td>
            line = "[%s]" % line.split("JavaScript:Book_Confirm(")[1].split(");")[0].replace("'", '"')
            line = json.loads(line)
            yyyymmdd = line[0]
            course = int(line[2])
            hhmm = line[4]

            #print("self.prefer_course_idx : %s, course : %s" % (self.prefer_course_idx, course))
            if self.prefer_course_idx != None and self.prefer_course_idx != course and self.prefer_course_idx != 0:
                continue
            form_data = {
                "yyyymmdd": yyyymmdd,
                "hhmm": hhmm,
                "course": course,
            }
            result_list.append(form_data)
        return result_list

    def get_user_info(self):
        url = "html/member/my_page.asp"
        html = self._get_text_with_cookie(url)

        self.user_name = html.split('<th><label for="joinName">')[1].split("<td>")[1].split("</td>")[0]
        self.user_phone = html.split('name="memb_hand_phone"')[1].split('value="')[1].split('"')[0]
        self.log("사용자 정보 조회 성공 : user_name:<%s>, user_phone:<%s>" % (self.user_name, self.user_phone))

    def check_is_reservation_time_available(self, yyyymmdd):
        days_inc = 0
        if self.get_weekday(yyyymmdd) == "일":
            days_inc = 1
        if self.weekday == "토" or self.weekday == "일":
            possible_time = datetime.strptime("%s 135950" % (yyyymmdd,), "%Y%m%d %H%M%S") - timedelta(days=17+days_inc)
        ##elif self.weekday == "일":
        ##    possible_time = datetime.strptime("%s 135955" % (yyyymmdd,), "%Y%m%d %H%M%S") - timedelta(days=16-days_inc)
        else:
            #possible_time = datetime.strptime("%s 235950" % (yyyymmdd,), "%Y%m%d %H%M%S") - timedelta(days=29) ##### 2023.05.08 <-- 예약 시간 변경
            possible_time = datetime.strptime("%s 095945" % (yyyymmdd,), "%Y%m%d %H%M%S") - timedelta(days=29)
        now_time = datetime.now()
        if possible_time < now_time:
            return True
        time_gab = possible_time - now_time
        day = time_gab.days
        hh = int(time_gab.seconds / 3600)
        mm = int((time_gab.seconds % 3600) / 60)
        ss = time_gab.seconds % 60

        if day == 0:
            info_str = "[대기중] 예약가능시간:%s, 남은시간:(%d시 %02d분 %02d초)" % (possible_time, hh, mm, ss)
        else:
            info_str = "[대기중] 예약가능시간:%s, 남은시간:(%d일 %02d시 %02d분 %02d초)" % (possible_time, day, hh, mm, ss)

        if self.ln_print == 0:
            self.log("예약이 가능한 시점까지 대기합니다.")
        if self.ln_print % 300 == 0:
            self.log(info_str)
        elif hh == 0 and mm == 0 and 10 > ss > 1:
            self.log(info_str)

        self.ln_print += 1
        return False

    def get_list(self, _yyyymmdd=None):
        b_checked = False
        selected_list = []
        if _yyyymmdd == None:
            _yyyymmdd = self.yyyymmdd
        for yyyymmdd in _yyyymmdd:
            if self.check_is_reservation_time_available(yyyymmdd) is False:
                continue
            b_checked = True
            list_url = "html/reservation/reservation_01_01_01.asp"
            list_data = {
                "book_date_bd": yyyymmdd,
                "book_date_be": "",
                "book_crs": "",
                "book_crs_name": "",
                "book_time": "",
            }
            self.log("START scan list : yyyymmdd=%s" % (yyyymmdd,))
            list_url = self._make_url(list_url)
            res = requests.post(list_url, headers=self._make_http_heades(), data=list_data)
            res.encoding = "UTF-8"
            result = res.text

            self.log("download page : url=%s, result_length=%s, post_payload=%s" % (list_url, len(result), list_data))

            items = self.__parse_course(result)
            if len(items) == 0:
                stat_report = self.add_try_history()
                if stat_report is not None:
                    self.log(stat_report)
                self.log("END scan #1 : yyyymmdd=%s" % (yyyymmdd,))
                return selected_list
            selected_list.append(self.select_time(items, yyyymmdd))

        if b_checked is True:
            self.log("END scan #2 : yyyymmdd=%s" % (yyyymmdd,))
        return selected_list

    def do_cancel(self, yyyymmdd, hhmm, int_course):
        url = "html/reservation/reservation_02_01_ok.asp"
        form_data = {
            "book_date" : yyyymmdd,
            "book_crs" : int_course,
            "book_crs_name" : "",
            "book_time" : hhmm,
            "rec_name" : "",
        }
        result = self._post_text_with_cookie(url, form_data)
        return True

    def do_book(self, selected_dict: dict):
        if selected_dict is None:
            return False
        form_data = {
            "book_date": selected_dict["yyyymmdd"],
            "book_crs": selected_dict["course"],
            "book_crs_name": self.get_course_mapping(selected_dict["course"]),
            "book_time": selected_dict["hhmm"],
            "REC_NAME": "",
            "REC_PHONE": "",
            "dong1_name": self.user_name,
            "REC_NAME1": "",
            "dong1_tele": self.user_phone,
            "dong2_name": "",
            "dong2_tele": "",
            "dong3_name": "",
            "dong3_tele": "",
            "dong4_name": "",
            "dong4_tele": "",
        }
        #printDict(form_data)
        url = "html/reservation/reservation_01_ok1.asp"
        #return True
        result = self._post_text_with_cookie(url, form_data)
        # for keyname in result_dict.keys():
        if 'reservation_02_01' in result:
            return True
        return False

    def make_login_data(self):
        login_url = "html/member/login_ok.asp"
        login_data = {
            "memb_passpw": "",
            "memb_inet_no": self.uid,
            "memb_inet_pass": self.passwd
        }
        return login_data, login_url

    def do_login(self):
        login_data, login_url = self.make_login_data()
        result = self._login(login_data, login_url)

        if "접속이 10분간 제한됩니다." in result:
            self.log("비밀번호 연속 오류로 인하여 10분간 로그인이 차단 되었습니다. : id=%s" % (self.uid,), loglv=CRITICAL)
            return False
        if "location.replace('/index.asp');" in result:
            self.log("로그인 시도 성공 : id=%s" % (self.uid,))
            return True
        self.login_cookie = None
        self.log("로그인 시도 실패 : id=%s" % (self.uid,))
        return False


    def init_auto_login(self):
        make_cookie_fn = None
        login_data, login_url = self.make_login_data()
        self.start_auto_login(login_data, login_url, make_cookie_fn)