import concurrent.futures
import time
from typing import List

import requests

from chinjucc.http_helper import ChinjuccHttp
from chinjucc.models.course_list_model import ChinjuccCourseListModel
from common.order_model import OrderReservation
from chinjucc.parser_header import ChinjuccParserHeader

from common.cc_logging import Logging


class ChinjuccBook:
    def __init__(self) -> None:
        super().__init__()

    def do_book_multi(self, course_list: List[ChinjuccCourseListModel], order: OrderReservation,
                      session: requests.sessions.Session) -> bool:
        idx_ok = None
        thread_list = []
        for idx, course in enumerate(course_list):
            executor = concurrent.futures.ThreadPoolExecutor()
            h_thread = executor.submit(self.do_book, course, order, session)
            thread_list.append(h_thread)
            time.sleep(0.033334)
        for idx, h_thread in enumerate(thread_list):
            if h_thread.result() is True:
                idx_ok = idx
        if idx_ok is None:
            return False
        return True

    def do_book(self,
                course: ChinjuccCourseListModel,
                order: OrderReservation,
                session: requests.sessions.Session) -> bool:
        try:
            form_data_step2 = self.do_book_step1(course, order, session)
            self.do_book_step2(session, form_data_step2)
        except Exception as e:
            return False
        return True

    def do_book_step2(self,
                      session: requests.sessions.Session,
                      form_data: dict) -> None:
        url: str = ChinjuccHttp.make_url("Reservation/ReservationCheck.aspx")
        result = session.post(url, data=form_data)

        Logging.log("예약하기 step2 서버 응답결과 : status_code=%d, sz_payload=%d" % (result.status_code, len(result.text)))

        Logging.log_as_file(result.text, tag="step2_예약결과")
        Logging.log_as_file(url, tag="step2_예약_url")
        Logging.log_as_file(form_data, tag="step2_예약_data")
        self.check_result_step2(result.text)

    def do_book_step1(self,
                      course: ChinjuccCourseListModel,
                      order: OrderReservation,
                      session: requests.sessions.Session) -> dict:

        url: str = ChinjuccHttp.make_url("Reservation/ReservationCheck.aspx")
        result = session.post(url, data=course.form_data)
        html = result.text

        Logging.log("예약하기 step1 서버 응답결과 : status_code=%d, sz_payload=%d" % (result.status_code, len(result.text)))

        Logging.log_as_file(result.text, tag="step1_예약결과")
        Logging.log_as_file(url, tag="step1_예약_url")
        Logging.log_as_file(course.form_data, tag="step1_예약_data")
        self.check_result_step1(html, course.form_data)

        eventargument = ChinjuccParserHeader.parser_dotnet_cache(html, "__EVENTARGUMENT")
        viewstate = ChinjuccParserHeader.parser_dotnet_cache(html, "__VIEWSTATE")
        viewstategenerator = ChinjuccParserHeader.parser_dotnet_cache(html, "__VIEWSTATEGENERATOR")

        return {
            "__EVENTTARGET": "ctl00$ContentPlaceHolder1$lbtOK",
            "__EVENTARGUMENT": eventargument,
            "__VIEWSTATE": viewstate,
            "__VIEWSTATEGENERATOR": viewstategenerator,
            "strReserveDate": course.form_data["strReserveDate"],
            "strReserveTime": course.form_data["strReserveTime"],  # hhmm
            "strCourseCode": course.form_data["strCourseCode"],  # 11 or 22...
            "strDayGubun": course.form_data["strDayGubun"],
            "strReserveType": course.form_data["strReserveType"],
            "strSeq": course.form_data["strSeq"],
            "strHole": course.form_data["strHole"],
            "strBu": course.form_data["strBu"],
            "dtmChangeDate": "",
            "strChangeCode": "",
            "strChangeTime": "",
            "strChangeSeq": "",
            "strChangeDayGubun": "",
            "strChangeHole": "",
            "ctl00$ContentPlaceHolder1$ddlInwon": "4",
        }

    def check_result_step1(self, html: str, form_data: dict) -> None:
        if '다른 회원님께서 예약 중입니다' in html:
            Logging.log("다른 회원님께서 예약 중입니다, in step 1.1 다른 시간을 선택해 주십시요. %s %s" % (
                form_data["strReserveDate"], form_data["strReserveTime"]))
            raise Exception

        if "선택하신 시간은 다른 분께서 예약을 하셨습니다" in html:
            Logging.log("선택하신 시간은 다른 분께서 예약을 하셨습니다. in step 1.2 다른 시간을 선택해 주십시요. %s %s" % (
                form_data["strReserveDate"], form_data["strReserveTime"]))
            raise Exception

    def check_result_step2(self, html: str, form_data: dict) -> None:
        if "다른 회원님께서 예약 중입니다" in html:
            Logging.log("다른 회원님께서 예약 중입니다, in step 2.1 다른 시간을 선택해 주십시요. %s %s" % (
                form_data["strReserveDate"], form_data["strReserveTime"]))
            raise Exception

        if "선택하신 시간은 다른 분께서 예약을 하셨습니다" in html:
            Logging.log("선택하신 시간은 다른 분께서 예약을 하셨습니다. in step 2.2 다른 시간을 선택해 주십시요. %s %s" % (
                form_data["strReserveDate"], form_data["strReserveTime"]))
            raise Exception
