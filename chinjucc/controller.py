import datetime
import time

import requests

from chinjucc.book import ChinjuccBook
from chinjucc.course_list import ChinjuccCourseList
from chinjucc.http_helper import ChinjuccHttp
from chinjucc.login import ChinjuccLogin
from common.order_model import OrderReservation
from common.cc_logging import Logging
from define import ROBOT_VERSION, UPDATE_DATE, AUTHOR
from dialog import logWindow


class ChinjuccController:
    order: OrderReservation
    course_list: ChinjuccCourseList
    login: ChinjuccLogin
    session: requests.sessions.Session
    http_helper: ChinjuccHttp
    book: ChinjuccBook
    last_login: datetime.datetime

    def __init__(self, order: OrderReservation) -> None:
        super().__init__()
        self.order = order
        self.login = ChinjuccLogin(order)
        self.course_list = ChinjuccCourseList()
        self.http_helper = ChinjuccHttp()
        self.book = ChinjuccBook()
        self.last_login = None

        self.site_name = "진주CC"
        self.site_addr = "https://www.chinjucc.co.kr/"

    def _need_login(self) -> bool:
        if self.last_login is None:
            return True
        timediff = datetime.datetime.now() - self.last_login
        if timediff.seconds > 600:
            return True
        return False

    def run(self) -> bool:
        time.sleep(1)
        while True:
            if self._need_login() is True:
                self.session = self.login.do_login()
                self.last_login = datetime.datetime.now()
            try:
                course_list = self.course_list.get_list(self.order, self.session)
                result = self.book.do_book_multi(
                    course_list,
                    self.order,
                    self.session)
                if result is True:
                    Logging.log("예약 성공, 프로그램 종료 합니다")
                    return True
            except Exception as e:
                continue
            time.sleep(5)

    def log_window_th(self, app):
        self.log_window = logWindow()
        Logging.set_log_window(self.log_window)
        self.log_window.resize(640, 480)
        self.log_window.show()
        self.print_init_log()
        app.exec_()

    def get_course_mapping(self, course_info):
        if course_info == None:
            return "임의의코스"
        course_dict = {
            "임의의코스": 0,
            "남강": 11,
            "촉석": 22,
        }
        # 이름으로 들어올 경우 숫자값 반환
        if course_info in course_dict.keys():
            return course_dict[course_info]

        if type(course_info) == str:
            try:
                course_info = int(course_info)
            except:
                pass

        # 숫자값으로 들어올 경우 이름 반환
        for course_name in course_dict.keys():
            if course_dict[course_name] == course_info:
                return course_name

        return None

    def print_init_log(self):
        Logging.log("%s<%s> 자동 예약 프로그램 기동 성공" % (self.site_name, self.site_addr))
        Logging.log("version=%s date=%s, author=%s" % (ROBOT_VERSION, UPDATE_DATE, AUTHOR))
        Logging.log("  -- 사이트 이름 : %s <%s>" % (self.site_name, self.site_addr))
        Logging.log("  -- 가입자 계정 : %s" % (self.order.uid,))
        Logging.log("  -- 가입자 비밀번호 : %s" % ("*" * len(self.order.passwd),))

        Logging.log("  -- 예약 일자 : %s (%s)" % (self.order.yyyymmdd, self.get_weekday(self.order.yyyymmdd)))
        Logging.log("  -- 예약 시간 : %s" % (self.order.get_prefer_time(),))
        #course_mapping = self.get_course_mapping(self.prefer_course_idx)
        #if hasattr(self, "no_caddy"):
        #    if self.no_caddy is True:
        #        course_mapping += '(노캐디)'
        #Logging.log("  -- 예약 코스 : %s" % (course_mapping,))

    def get_weekday(self, yyyymmdd):
        week_name = ['월', '화', '수', '목', '금', '토', '일']
        weekday = datetime.datetime.strptime("%s 12:00:00" % yyyymmdd, "%Y%m%d %H:%M:%S").weekday()
        return week_name[weekday]
