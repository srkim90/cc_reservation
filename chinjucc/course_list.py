import time
from datetime import datetime, timedelta
from operator import itemgetter
from typing import List, Tuple

import requests

from chinjucc.http_helper import ChinjuccHttp
from chinjucc.models.course_list_model import ChinjuccCourseListModel
from common.common_http_helper import CommonHttpHelper
from common.order_model import OrderReservation
from chinjucc.parser_header import ChinjuccParserHeader
from common.cc_logging import Logging


class ChinjuccCourseList:
    http_helper: ChinjuccHttp

    def __init__(self) -> None:
        super().__init__()
        self.http_helper = ChinjuccHttp()

    def get_auth_login_ranges(self) -> List[List[str]]:
        return [
            ["08:55", "09:15"],
        ]

    def check_is_reservation_time_available(self, yyyymmdd):
        # possible_time = datetime.strptime("%s 085955" % (yyyymmdd,), "%Y%m%d %H%M%S") - timedelta(days=21)
        s_hhmmss = self.get_auth_login_ranges()[0][0].replace(":", "") + "00"
        possible_time = datetime.strptime("%s %s" % (yyyymmdd, s_hhmmss), "%Y%m%d %H%M%S") - timedelta(days=21)
        now_time = datetime.now()
        if possible_time < now_time:
            return True
        time_gab = possible_time - now_time
        day = time_gab.days
        hh = int(time_gab.seconds / 3600)
        mm = int((time_gab.seconds % 3600) / 60)
        ss = time_gab.seconds % 60

        if day == 0:
            n_wait = 60
            info_str = "[대기중] 예약가능시간:%s, 남은시간:(%d시 %02d분 %02d초)" % (possible_time, hh, mm, ss)
        else:
            n_wait = 600
            info_str = "[대기중] 예약가능시간:%s, 남은시간:(%d일 %02d시 %02d분 %02d초)" % (possible_time, day, hh, mm, ss)

        Logging.log(info_str)
        time.sleep(n_wait)
        return False

    def get_list(self,
                 order: OrderReservation,
                 session: requests.sessions.Session) -> List[ChinjuccCourseListModel]:


        if self.check_is_reservation_time_available(order.yyyymmdd) is False:
            raise ModuleNotFoundError
        form_data, heads = self.get_list_step_1(order, session)
        return self.get_list_step_2(form_data, heads, order, session)

    def get_list_step_2(self,
                        form_data: str,
                        heads: dict,
                        order: OrderReservation,
                        session: requests.sessions.Session) -> List[ChinjuccCourseListModel]:
        response = session.post(ChinjuccHttp.make_url("Reservation/ReservationTimeList.aspx"), data=form_data, headers=heads)
        html = response.text

        viewstate = ChinjuccParserHeader.parser_dotnet_cache(html, "__VIEWSTATE")
        viewstategenerator = ChinjuccParserHeader.parser_dotnet_cache(html, "__VIEWSTATEGENERATOR")

        items: List[ChinjuccCourseListModel] = []
        for line in html.split('\n'):
            if 'javascript:ReserveCheck(' not in line or '예약가능' not in line:
                continue
            items.append(self.__parse_course(line, viewstate, viewstategenerator))
        return self.select_time(items, order.yyyymmdd, order.get_prefer_time(), 45)

    def get_list_step_1(self, order: OrderReservation, session: requests.sessions.Session) -> Tuple[str, dict]:
        first_url = self.http_helper.make_url("Reservation/Reservation.aspx")

        result = session.get(first_url)
        html = result.text

        eventtarget = ChinjuccParserHeader.parser_dotnet_cache(html, "__EVENTTARGET")
        eventargument = ChinjuccParserHeader.parser_dotnet_cache(html, "__EVENTARGUMENT")
        viewstate = ChinjuccParserHeader.parser_dotnet_cache(html, "__VIEWSTATE")
        viewstategenerator = ChinjuccParserHeader.parser_dotnet_cache(html, "__VIEWSTATEGENERATOR")
        eventvalidation = ChinjuccParserHeader.parser_dotnet_cache(html, "__EVENTVALIDATION")

        form_data = {
            "__EVENTTARGET": eventtarget,
            "__EVENTARGUMENT": eventargument,
            "__VIEWSTATE": viewstate,
            "__VIEWSTATEGENERATOR": viewstategenerator,
            "__EVENTVALIDATION": eventvalidation,
            "strYY": order.yyyymmdd[0:4],
            "strMM": order.yyyymmdd[4:6],
            "strReserveDate": "%s-%s-%s" % (order.yyyymmdd[0:4], order.yyyymmdd[4:6], order.yyyymmdd[6:8]),
            "strDayGubun": "1",
            "dtmChangeDate": "",
            "strChangeCode": "",
            "strChangeTime": "",
            "strChangeSeq": "",
            "strChangeDayGubun": "",
            "strChangeHole": "",
            "strReserveType": "1",
            "strReserveTime": "",
            "strCourseCode": "",
            "hdnSeq": "",
            "choice_hole": "",
            "DaegiYN1": "",
            "CadYN": "",
            "ctl00$ContentPlaceHolder1$htbArgs": "INIT"
        }
        heads = ChinjuccHttp.http_heads()
        heads["referer"] = "https://www.chinjucc.co.kr/Reservation/Reservation.aspx"
        return (CommonHttpHelper.dict_url_encode(form_data), heads)


    def __parse_course(self, line: str, viewstate: str, viewstategenerator: str) -> ChinjuccCourseListModel:
        # <td>13:18</td><td>18홀</td><td><a href="javascript:ReserveCheck('2021-09-04', '11', '1318', '034', '2', '18', '2');" class="cal21">예약가능</a></td>
        reserveCheck = line.split('javascript:ReserveCheck(')[1].split(");")[0].replace("'", "").replace(" ", "").split(
            ",")
        strReserveDate = reserveCheck[0]
        strReserveTime = reserveCheck[2]
        strCourseCode = reserveCheck[1]
        strDayGubun = reserveCheck[4]
        strReserveType = "1"
        strSeq = reserveCheck[3]
        strHole = reserveCheck[5]
        strBu = reserveCheck[6]

        form_data = {
            "__VIEWSTATE": viewstate,
            "__VIEWSTATEGENERATOR": viewstategenerator,
            "strReserveDate": strReserveDate,
            "strReserveTime": strReserveTime,  # hhmm
            "strCourseCode": strCourseCode,  # 11 or 22...
            "strDayGubun": strDayGubun,
            "strReserveType": strReserveType,
            "strSeq": strSeq,
            "strHole": strHole,
            "strBu": strBu,
            "CadYN": "",
            "dtmChangeDate": "",
            "strChangeCode": "",
            "strChangeTime": "",
            "strChangeSeq": "",
            "strChangeDayGubun": "",
            "strChangeHole": "",
        }
        return ChinjuccCourseListModel(
            strReserveDate.replace("-", ""),
            strReserveTime,
            strCourseCode,
            form_data
        )

    def select_time(self, course_list: List[ChinjuccCourseListModel], yyyymmdd, prefer_time, n_result=1):
        if len(course_list) == 0:
            return None
        prefer_date = datetime.strptime("%s%s00" % (yyyymmdd, prefer_time.replace(":", "")),
                                        "%Y%m%d%H%M%S").timestamp()
        time_gap = 3600 * 128  # 의미없는 큰 시간
        select_at: List[ChinjuccCourseListModel]
        Logging.log("=========================================")
        Logging.log("예약 가능한 시간대 리스트")
        Logging.log("-----------------------------------------")
        all_list = []
        for idx, item in enumerate(course_list):
            add_info = ""
            Logging.log("  - %s %s %s(%s) %s" % (
            item.yyyymmdd, item.hhmm, self.get_course_mapping(item.course), item.course, add_info))
            # if self.check_already_booked_time(item["yyyymmdd"], item["hhmm"], item["course"]) is True:
            #     Logging.log("SKIP")
            #     continue
            date_at = datetime.strptime("%s%s00" % (item.yyyymmdd, item.hhmm), "%Y%m%d%H%M%S").timestamp()
            time_gap_at = abs(prefer_date - date_at)
            if time_gap > time_gap_at:
                time_gap = time_gap_at
            all_list.append({
                "time_gap": time_gap_at,
                "item": item
            })
        Logging.log("-----------------------------------------")
        Logging.log("선택 된 시간")

        _all_list = sorted(all_list, key=itemgetter('time_gap'), reverse=False)
        all_list = []
        for item in _all_list:
            all_list.append(item["item"])
        if len(all_list) >= n_result:
            select_at = all_list[0:n_result - 1]
        else:
            select_at = all_list
        for item in select_at:
            Logging.log("  - %s %s %s" % (item.yyyymmdd, item.hhmm, self.get_course_mapping(item.course)))
        Logging.log("=========================================")
        return select_at

    def get_course_mapping(self, course_info):
        if course_info == None:
            return "임의의코스"
        course_dict = {
            "임의의코스": 0,
            "남강": 11,
            "촉석": 22,
        }
        # 이름으로 들어올 경우 숫자값 반환
        if course_info in course_dict.keys():
            return course_dict[course_info]

        if type(course_info) == str:
            try:
                course_info = int(course_info)
            except:
                pass

        # 숫자값으로 들어올 경우 이름 반환
        for course_name in course_dict.keys():
            if course_dict[course_name] == course_info:
                return course_name

        return None
