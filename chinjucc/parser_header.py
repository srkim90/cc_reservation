

class ChinjuccParserHeader:
    def __init__(self) -> None:
        super().__init__()

    @staticmethod
    def parser_dotnet_cache(html, check_name):
        for line in html.split("\n"):
            if check_name not in line:
                continue
            value = line.split('value="')[-1].split('"')[0]
            return value
        return None