import urllib
from datetime import datetime
from http import HTTPStatus



from typing import Tuple

import requests

from chinjucc.http_helper import ChinjuccHttp
from common.order_model import OrderReservation
from common.cc_logging import Logging
from common.cc_loglevel import *


class ChinjuccLogin:
    http_helper : ChinjuccHttp
    uid: str
    passwd: str
    last_login : datetime

    def __init__(self, order: OrderReservation) -> None:
        super().__init__()
        self.uid = order.uid
        self.passwd = order.passwd
        self.http_helper = ChinjuccHttp()
        self.session = requests.sessions.Session()
        self.last_login = None

    def make_login_data(self) -> Tuple[dict, str, dict]:
        login_url = "Join/Login.aspx"
        login_data = {
            "__EVENTTARGET": "ctl00$ContentPlaceHolder1$lbtLogin",
            "__EVENTARGUMENT": "",
            "__VIEWSTATE": "/wEPDwUJNjAyMTM0NzE1DxYCHgdQcmV2VXJsBQYvbWFpbi8WAmYPZBYCAgMPZBYGAgEPZBYIAgEPDxYCHgtOYXZpZ2F0ZVVybAUfaHR0cHM6Ly93d3cuY2hpbmp1Y2MuY28ua3IvbWFpbmRkAgMPDxYCHwEFIGh0dHBzOi8vd3d3LmVhc3RoaWxscy5jby5rci9tYWluZGQCBQ8WAh4EVGV4dAV0PGxpPjxhIGhyZWYgPSAiL0pvaW4vTG9naW4uYXNweCI+IOuhnOq3uOyduCA8L2E+PC9saT4gICA8bGk+PGEgaHJlZiA9ICIvSm9pbi9Kb2luU3RlcDEuYXNweCI+IO2ajOybkOqwgOyehSA8L2E+PC9saT5kAgcPFgIfAgWfDjxsaSBjbGFzcz0ibWVudTAxIj48YSBocmVmPSIvQWJvdXQvSW50cm9kdWN0aW9uLmFzcHgiPu2BtOufveyGjOqwnDwvYT4KICA8b2wgY2xhc3M9InNtZW51Ij4NCgkJCQkJPGxpPjxhIGhyZWY9Ii9BYm91dC9JbnRyb2R1Y3Rpb24uYXNweCI+7YG065+97IaM6rCcPC9hPjwvbGk+DQoJCQkJCTxsaT48YSBocmVmPSIvQWJvdXQvQ2VvLmFzcHgiPuyduOyCrOunkDwvYT48L2xpPg0KCQkJCQk8bGk+PGEgaHJlZj0iL0Fib3V0L0hpc3RvcnkuYXNweCI+7Jew7ZiBPC9hPjwvbGk+DQoJCQkJCTxsaT48YSBocmVmPSIvQWJvdXQvTG9jYXRpb24uYXNweCI+7Jik7Iuc64qUIOq4uDwvYT48L2xpPgogIDwvb2w+CjwvbGk+CjxsaSBjbGFzcz0ibWVudTAyIj48YSBocmVmPSIvR3VpZGUvR3VpZGUuYXNweCI+7Jq07JiB7JWI64K0PC9hPgogIDxvbCBjbGFzcz0ic21lbnUiPg0KCQkJCQk8bGk+PGEgaHJlZj0iL0d1aWRlL0d1aWRlLmFzcHgiPuydtOyaqeyViOuCtDwvYT48L2xpPg0KCQkJCQk8bGk+PGEgaHJlZj0iL0d1aWRlL0ZlZXMuYXNweCI+7J207Jqp7JqU6riIPC9hPjwvbGk+DQoJCQkJCTxsaT48YSBocmVmPSIvR3VpZGUvUmVzZXJ2ZUluZm8uYXNweCI+7JiI7JW97JWI64K0PC9hPjwvbGk+DQoJCQkJCTxsaT48YSBocmVmPSIvR3VpZGUvQ2xhdXNlLmFzcHgiPuydtOyaqeyVveq0gDwvYT48L2xpPgogIDwvb2w+CjwvbGk+CjxsaSBjbGFzcz0ibWVudTAzIj48YSBocmVmPSIvQ291cnNlL0ludHJvLmFzcHgiPuy9lOyKpOyGjOqwnDwvYT4KICA8b2wgY2xhc3M9InNtZW51Ij4NCgkJCQkJPGxpPjxhIGhyZWY9Ii9Db3Vyc2UvSW50cm8uYXNweCI+7L2U7Iqk7IaM6rCcPC9hPjwvbGk+DQoJCQkJCTxsaT48YSBocmVmPSIvQ291cnNlL0NvdXJzZURlc2lnbmVyLmFzcHgiPuy9lOyKpOqzteueteuPhDwvYT48L2xpPg0KCQkJCQk8bGk+PGEgaHJlZj0iL0NvdXJzZS9TY29yZUNhcmQuYXNweCI+7Iqk7L2U7Ja07Lm065OcPC9hPjwvbGk+DQoJCQkJCTxsaT48YSBocmVmPSIvQ291cnNlL0xvY2FsUnVsZS5hc3B4Ij7roZzsu6zro7A8L2E+PC9saT4KICA8L29sPgo8L2xpPgo8bGkgY2xhc3M9Im1lbnUwNCI+PGEgaHJlZj0iL0ZhY2lsaXRpZXMvRmFjaWxpdGllczAxLmFzcHgiPuyLnOyEpOyViOuCtDwvYT4KICA8b2wgY2xhc3M9InNtZW51Ij4NCgkJCQkJPGxpPjxhIGhyZWY9Ii9GYWNpbGl0aWVzL0ZhY2lsaXRpZXMwMS5hc3B4Ij7si5zshKTshozqsJw8L2E+PC9saT4NCgkJCQkJPGxpPjxhIGhyZWY9Ii9GYWNpbGl0aWVzL0ZhY2lsaXRpZXMwMi5hc3B4Ij7si5zshKTslYjrgrQ8L2E+PC9saT4KICA8L29sPgo8L2xpPgo8bGkgY2xhc3M9Im1lbnUwNSI+PGEgaHJlZj0iL0JvYXJkL0JvYXJkTGlzdC5hc3B4P2NmZ2lkPW5vdGljZSI+7KCV67O066eI64u5PC9hPgogIDxvbCBjbGFzcz0ic21lbnUiPg0KCQkJCQk8bGk+PGEgaHJlZj0iL0JvYXJkL0JvYXJkTGlzdC5hc3B4P2NmZ2lkPW5vdGljZSI+6rO17KeA7IKs7ZWtPC9hPjwvbGk+DQoJCQkJCTxsaT48YSBocmVmPSIvQm9hcmQvQm9hcmRMaXN0LmFzcHg/Y2ZnaWQ9ZnJlZWJvYXJkIj7qsozsi5ztjJA8L2E+PC9saT4NCgkJCQkJPGxpPjxhIGhyZWY9Ii9Cb2FyZC9HYWxsZXJ5TGlzdDIuYXNweD9jZmdpZD1jdXN0b21lcmdhbGxlcnkiPu2PrO2GoOqwpOufrOumrDwvYT48L2xpPg0KCQkJCQk8bGk+PGEgaHJlZj0iL0JvYXJkL0JvYXJkTGlzdC5hc3B4P2NmZ2lkPXBkcyI+7J6Q66OM7IukPC9hPjwvbGk+DQoJCQkJCTxsaT48YSBocmVmPSIvQm9hcmQvQm9hcmRMaXN0LmFzcHg/Y2ZnaWQ9am9pbiI+7KGw7J246rKM7Iuc7YyQPC9hPjwvbGk+CiAgPC9vbD4KPC9saT4KZAIDDxYCHwIF2wE8ZGl2IGlkPSJ2aXN1YWwiPgoJPGRpdiBjbGFzcz0idmlzdWFsX2luIj4KICAgIAk8ZGl2IGNsYXNzPSJqb2luX2JnIj4KICAgICAgICAJPHAgY2xhc3M9InZpc190dDEiPkpPSU48L3A+CiAgICAgICAgICAgIDxwIGNsYXNzPSJ2aXNfdHQyIj5UaGUgTXl0aCBPZiBHb2xmIEF0IFRoZSBDSElOSlUgQ0MgV2lsbCBCZWdpbjwvcD4KICAgICAgICA8L2Rpdj4KICAgIDwvZGl2Pgo8L2Rpdj5kAgUPFgIfAgXPAw0KCQk8bGk+PGEgaHJlZj0iL0pvaW4vTG9naW4uYXNweCI+66Gc6re47J24PC9hPjwvbGk+DQoJCTxsaT48YSBocmVmPSIvSm9pbi9Kb2luU3RlcDEuYXNweCI+7ZqM7JuQ6rCA7J6FPC9hPjwvbGk+DQoJCTxsaT48YSBocmVmPSIvSm9pbi9Bc2tJRG5QVy5hc3B4Ij7slYTsnbTrlJQv67mE67CA67KI7Zi4IOywvuq4sDwvYT48L2xpPg0KCQk8bGk+PGEgaHJlZj0iL0pvaW4vV2l0aGRyYXdhbC5hc3B4Ij7tmozsm5Dtg4jth7Q8L2E+PC9saT4NCgkJPGxpPjxhIGhyZWY9Ii9TaXRlL0FncmVlbWVudC5hc3B4Ij7snbTsmqnslb3qtIA8L2E+PC9saT4NCgkJPGxpPjxhIGhyZWY9Ii9TaXRlL1ByaXZhdGVQb2xpY3kuYXNweCI+6rCc7J247KCV67O07LKY66as67Cp7LmoPC9hPjwvbGk+DQoJCTxsaT48YSBocmVmPSIvU2l0ZS9FbWFpbFBvbGljeS5hc3B4Ij7snbTrqZTsnbzrrLTri6jsiJjsp5HqsbDrtoA8L2E+PC9saT5kZBt6sSrJo6AIGHtTbXR56YDLkDf/OewfBtSJHl+Brhq7",
            "__VIEWSTATEGENERATOR": "E3A54082",
            "__EVENTVALIDATION": "/wEdAAcKS14/RylRLbwSlo/bKQ4Vuqd4RWw5YZhOfgnuYJELYbQxWZkQxLAjkslCygpWavJ3YaFcLR3uPm6Mb620jW+eBT93GWmvjFo94Li2aAlKRXjjswiyhm+g6KodwobC/fNAA3kIsIGxY908if7afHakoylwcHCwdX6pjAgLptvs4r139X507U85jYV+1/AdfOA=",
            "ctl00$ContentPlaceHolder1$rbtLoginGroup": 160,
            "ctl00$ContentPlaceHolder1$txtUserID": self.uid,
            "ctl00$ContentPlaceHolder1$txtPassword": self.passwd,
        }
        add_headers = ChinjuccHttp.http_heads()
        return urllib.parse.urlencode(login_data), login_url, add_headers

    def _login(self, login_data, login_url, add_headers=None):
        login_url = ChinjuccHttp.make_url(login_url)

        headers = ChinjuccHttp.http_heads()
        # if type(login_data) == str:
        #     headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
        #     headers['Host'] = "www.geojeview.co.kr"
        #     headers['Origin'] = "https://www.geojeview.co.kr"
        #     headers['Referer'] = "https://www.geojeview.co.kr/auth/login"
        #     headers['sec-ch-ua'] = 'Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"'
        #     headers['sec-ch-ua-mobile'] = "?0"
        #     headers['Sec-Fetch-Dest']  = "empty"
        #     headers['Sec-Fetch-Mode'] = "cors"
        #     headers['Sec-Fetch-Site'] = "same-origin"
        #     headers['X-Requested-With'] = "XMLHttpRequest"

        if add_headers is not None:
            for key_name in add_headers.keys():
                headers[key_name] = add_headers[key_name]


        res = self.session.post(login_url, data=login_data, headers=headers)


        # print("response : %s\n%s" % (res.status_code, self.html_header_to_json(res.headers)))
        # print("request  : %s" % self.html_header_to_json(res.request.headers))
        # print(res.text)
        # sleep(999)
        if res.status_code == HTTPStatus.OK or res.status_code == HTTPStatus.ACCEPTED:
            if "Set-Cookie" not in res.headers.keys():
                return None
            self.login_cookie = res.headers["Set-Cookie"]  # self.add_cookies([res.headers["Set-Cookie"], org_cookie])
            self.login_time = datetime.now()
            # self.log(self.login_cookie)
            # print(self.login_cookie)
            # sleep(9999)

            result = res.text  # .encode("utf-8").decode("utf-8")
            return result
        else:
            return None

    def check_login_success(self, html):
        if "location.href = '/main/';" in html:
            self.last_login = datetime.now()
            return True
        return False

    def do_login(self) -> requests.sessions.Session:
        self.session = requests.sessions.Session()
        login_data, login_url, add_headers = self.make_login_data()

        result = self._login(login_data, login_url, add_headers=add_headers)
        if result is None:
            raise Exception
        if 'alert("비밀번호가 일치하지 않습니다.");' in result:
            Logging.log("비밀번호 오류로 인하여 로그인이 실패 하였습니다. : id=%s" % (self.uid,), loglv=CRITICAL)
            raise Exception
        if self.check_login_success(result) is True:
            Logging.log("로그인 시도 성공 : id=%s" % (self.uid,))
            #self.chinjucc_cookie_helper()
        return self.session
        # self.login_cookie = None
        # Logging.log("로그인 시도 실패 : id=%s" % (self.uid,))
        # return False