
class ChinjuccCourseListModel:
    yyyymmdd: str
    hhmm: str
    course: str
    form_data: dict

    def __init__(self, yyyymmdd: str, hhmm: str, course: str, form_data: dict) -> None:
        super().__init__()
        self.yyyymmdd = yyyymmdd
        self.hhmm = hhmm
        self.course = course
        self.form_data = form_data