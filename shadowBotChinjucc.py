import threading
import time

from define import chinjucc_pre_defined_reserve_time_list
from utils import *
from logging import *
import concurrent.futures
from shadowBotBase import shadowBotBase
from datetime import datetime, timedelta


class shadowBotChinjucc(shadowBotBase):
    def __init__(self, site_name, user_name, user_phone, yyyymmdd, prefer_time, prefer_course_idx, uid, passwd):
        self.ln_print = 0
        site_addr = "https://www.chinjucc.co.kr"
        # prefer_course_idx = self.get_course_mapping(prefer_course_idx)
        shadowBotBase.__init__(self, site_name, site_addr, user_name, user_phone, yyyymmdd, prefer_time, uid, passwd,
                               prefer_course_idx)
        self.auth_login_ranges = [
            ["08:55", "09:15"],
            #            ["09:55", "23:15"],
        ]
        self.login_timeout_sec = 500
        if self.weekday == "토":
            next_yyyymmdd = (
                        datetime.strptime("%s 12:00:00" % yyyymmdd, "%Y%m%d %H:%M:%S") + timedelta(days=1)).strftime(
                "%Y%m%d")
            self.yyyymmdd.append(next_yyyymmdd)
        elif self.weekday == "일":
            next_yyyymmdd = (
                        datetime.strptime("%s 12:00:00" % yyyymmdd, "%Y%m%d %H:%M:%S") - timedelta(days=1)).strftime(
                "%Y%m%d")
            self.yyyymmdd.append(next_yyyymmdd)

    @staticmethod
    def get_course_list():
        return ["임의의코스", "남강", "촉석"]

    def check_cancel(self):
        if self.prefer_course_idx == None:
            return False
        # print("%s self.prefer_course_idx" % self.prefer_course_idx)
        last_idx = self.get_course_mapping("남강")
        if self.prefer_course_idx > last_idx:
            self.do_cancel(self.yyyymmdd[0], self.prefer_time.replace(":", ""),
                           self.prefer_course_idx - last_idx)  # 가야=1, 충무=2, 공룡=3
            return True
        return False

    def get_course_mapping(self, course_info):
        if course_info == None:
            return "임의의코스"
        course_dict = {
            "임의의코스": 0,
            "남강": 11,
            "촉석": 22,
        }
        # 이름으로 들어올 경우 숫자값 반환
        if course_info in course_dict.keys():
            return course_dict[course_info]

        if type(course_info) == str:
            try:
                course_info = int(course_info)
            except:
                pass

        # 숫자값으로 들어올 경우 이름 반환
        for course_name in course_dict.keys():
            if course_dict[course_name] == course_info:
                return course_name

        return None

    def chinjucc_cookie_helper(self):
        login_cookie = []
        checker = ["MemberAgree", "LoginCheck", "LoginGubun", "MemberGNo", "MemberName", "MemberMobile", "MemberID",
                   "MemberNo", "MemberGubun", "MemberGroup", "MemberDepo", "MemberEmail", "MemberBoard", "MemberCode",
                   "LastCheckTime", "AWSALB"]
        for idx, item in enumerate(self.login_cookie.split(";")):
            item = item.strip()
            f_exist = False
            for check_at in checker:
                if check_at in item:
                    f_exist = True
                    break
            if f_exist == True:
                # if idx != 0:
                #    login_cookie += "; "
                # login_cookie += item
                login_cookie.append(item)
        login_cookie_str = ""
        for check_at in checker:
            for item in login_cookie:
                if check_at in item:
                    if len(login_cookie_str) != 0:
                        login_cookie_str += "; "
                    login_cookie_str += item
                    break
        login_cookie_str = login_cookie_str.replace("path=/, ", "")
        self.login_cookie = login_cookie_str

    def get_user_info(self):
        url = "Join/MemberEdit.aspx"
        html = self._get_text_with_cookie(url, heads=self.chinjucc_http_heads())

        try:
            self.user_name = html.split('<span id="ctl00_ContentPlaceHolder1_lblUserName">')[1].split("</span>")[0]
            self.user_phone = html.split('" != strMobile) {')[0].split('"')[-1]
            self.log("사용자 정보 조회 성공 : user_name:<%s>, user_phone:<%s>" % (self.user_name, self.user_phone))
        except Exception as e:
            self.log("사용자 정보 조회 실패 : 로그인은 성공 했음으로 예약은 계속 진행")

    def check_is_reservation_time_available(self, yyyymmdd):
        # possible_time = datetime.strptime("%s 085955" % (yyyymmdd,), "%Y%m%d %H%M%S") - timedelta(days=21)
        s_hhmmss = self.auth_login_ranges[0][0].replace(":", "") + "00"
        possible_time = datetime.strptime("%s %s" % (yyyymmdd, s_hhmmss), "%Y%m%d %H%M%S") - timedelta(days=21)
        now_time = datetime.now()
        if possible_time < now_time:
            return True
        time_gab = possible_time - now_time
        day = time_gab.days
        hh = int(time_gab.seconds / 3600)
        mm = int((time_gab.seconds % 3600) / 60)
        ss = time_gab.seconds % 60

        if day == 0:
            info_str = "[대기중] 예약가능시간:%s, 남은시간:(%d시 %02d분 %02d초)" % (possible_time, hh, mm, ss)
        else:
            info_str = "[대기중] 예약가능시간:%s, 남은시간:(%d일 %02d시 %02d분 %02d초)" % (possible_time, day, hh, mm, ss)

        if self.ln_print == 0:
            self.log("예약이 가능한 시점까지 대기합니다.")
        if self.ln_print % 300 == 0:
            self.log(info_str)
        elif hh == 0 and mm == 0 and 10 > ss > 1:
            self.log(info_str)

        self.ln_print += 1
        return False

    def parser_dotnet_cache(self, html, check_name):
        for line in html.split("\n"):
            if check_name not in line:
                continue
            value = line.split('value="')[-1].split('"')[0]
            return value
        return None

    def get_list(self, _yyyymmdd=None):
        selected_list = []

        _yyyymmdd = self.yyyymmdd
        if type(_yyyymmdd) == list:
            _yyyymmdd = _yyyymmdd[0]

        if self.check_is_reservation_time_available(_yyyymmdd) is False:
            return

        first_url = "Reservation/Reservation.aspx"
        list_url = "Reservation/ReservationTimeList.aspx"
        html = self._get_text_with_cookie(first_url)

        eventtarget = self.parser_dotnet_cache(html, "__EVENTTARGET")
        eventargument = self.parser_dotnet_cache(html, "__EVENTARGUMENT")
        viewstate = self.parser_dotnet_cache(html, "__VIEWSTATE")
        viewstategenerator = self.parser_dotnet_cache(html, "__VIEWSTATEGENERATOR")
        eventvalidation = self.parser_dotnet_cache(html, "__EVENTVALIDATION")

        list_data = {
            "__EVENTTARGET": eventtarget,
            "__EVENTARGUMENT": eventargument,
            "__VIEWSTATE": viewstate,
            "__VIEWSTATEGENERATOR": viewstategenerator,
            "__EVENTVALIDATION": eventvalidation,
            "strYY": _yyyymmdd[0:4],
            "strMM": _yyyymmdd[4:6],
            "strReserveDate": "%s-%s-%s" % (_yyyymmdd[0:4], _yyyymmdd[4:6], _yyyymmdd[6:8]),
            "strDayGubun": "1",
            "dtmChangeDate": "",
            "strChangeCode": "",
            "strChangeTime": "",
            "strChangeSeq": "",
            "strChangeDayGubun": "",
            "strChangeHole": "",
            "strReserveType": "1",
            "strReserveTime": "",
            "strCourseCode": "",
            "hdnSeq": "",
            "choice_hole": "",
            "DaegiYN1": "",
            "CadYN": "",
            "ctl00$ContentPlaceHolder1$htbArgs": "INIT"
        }
        list_data = self._dict_url_encode(list_data)
        heads = self.chinjucc_http_heads()
        heads["referer"] = "https://www.chinjucc.co.kr/Reservation/Reservation.aspx"

        response = self._post_with_cookie(list_url, data=list_data, heads=heads)

        # aaaa = json.dumps(dict(response.request.headers), indent=4, sort_keys=True)
        # print(aaaa)
        items = []
        html = response.text
        self.log_as_file(html)
        for line in html.split('\n'):
            if 'javascript:ReserveCheck(' not in line or '예약가능' not in line:
                continue
            parsed_line = self.__parse_course(line, viewstate, viewstategenerator)
            items.append(parsed_line)
        select_time_item = self.select_time(items, _yyyymmdd, 45)
        if select_time_item is not None:
            if type(select_time_item) == list:
                selected_list = select_time_item
            else:
                selected_list.append(select_time_item)

        return selected_list

    def _logout(self):
        self._get_with_cookie("Join/LogOut.aspx")
        self.login_cookie = None

    def __parse_course(self, line, viewstate, viewstategenerator) -> dict:
        # <td>13:18</td><td>18홀</td><td><a href="javascript:ReserveCheck('2021-09-04', '11', '1318', '034', '2', '18', '2');" class="cal21">예약가능</a></td>
        reserveCheck = line.split('javascript:ReserveCheck(')[1].split(");")[0].replace("'", "").replace(" ", "").split(
            ",")
        strReserveDate = reserveCheck[0]
        strReserveTime = reserveCheck[2]
        strCourseCode = reserveCheck[1]
        strDayGubun = reserveCheck[4]
        strReserveType = "1"
        strSeq = reserveCheck[3]
        strHole = reserveCheck[5]
        strBu = reserveCheck[6]

        form_data = {
            "__VIEWSTATE": viewstate,
            "__VIEWSTATEGENERATOR": viewstategenerator,
            "strReserveDate": strReserveDate,
            "strReserveTime": strReserveTime,  # hhmm
            "strCourseCode": strCourseCode,  # 11 or 22...
            "strDayGubun": strDayGubun,
            "strReserveType": strReserveType,
            "strSeq": strSeq,
            "strHole": strHole,
            "strBu": strBu,
            "CadYN": "",
            "dtmChangeDate": "",
            "strChangeCode": "",
            "strChangeTime": "",
            "strChangeSeq": "",
            "strChangeDayGubun": "",
            "strChangeHole": "",
        }
        return {
            "yyyymmdd": strReserveDate.replace("-", ""),
            "hhmm": strReserveTime,
            "course": strCourseCode,
            "form_data": form_data
        }

    def do_cancel(self, yyyymmdd, hhmm, int_course):
        url = "html/reservation/reservation_02_01_ok.asp"
        form_data = {
            "book_date": yyyymmdd,
            "book_crs": int_course,
            "book_crs_name": "",
            "book_time": hhmm,
            "rec_name": "",
        }
        result = self._post_text_with_cookie(url, form_data)
        return True

    def do_book_multi(self, course_list: list):
        idx_ok = None
        thread_list = []
        for idx, course in enumerate(course_list):
            executor = concurrent.futures.ThreadPoolExecutor()
            hThread = executor.submit(self.do_book, course)
            thread_list.append(hThread)
            time.sleep(0.033334)
        for idx, hThread in enumerate(thread_list):
            if hThread.result() is True:
                idx_ok = idx
        if idx_ok is None:
            return False
        return course_list[idx_ok]

    def do_book(self, selected_dict: dict):
        if selected_dict is None:
            self.log("do_book Error. No 1")
            return False
        form_data = selected_dict["form_data"]
        url = "Reservation/ReservationCheck.aspx"

        heads = self.chinjucc_http_heads()
        heads["referer"] = "https://www.chinjucc.co.kr/Reservation/ReservationTimeList.aspx"
        result = self._post_with_cookie(url, data=form_data, heads=heads)

        self.log("예약하기 step1 서버 응답결과 : status_code=%d, sz_payload=%d" % (result.status_code, len(result.text)))

        self.log_as_file(result.text, tag="step1_예약결과")
        self.log_as_file(url, tag="step1_예약_url")
        self.log_as_file(heads, tag="step1_예약_heads")
        self.log_as_file(form_data, tag="step1_예약_data")
        self.log_as_file(self.login_cookie, tag="step1_예약_req_login_cookie")
        self.log_as_file("%s" % (self.html_header_to_json(result.headers),), tag="step1_예약_res_header")

        html = result.text

        if '다른 회원님께서 예약 중입니다' in result:
            self.log("다른 회원님께서 예약 중입니다, in step 1.1 다른 시간을 선택해 주십시요. %s %s" % (
            form_data["strReserveDate"], form_data["strReserveTime"]))
            # self.try_login()
            # self.add_already_booked_time(form_data["strReserveDate"], form_data["strReserveTime"], form_data["strCourseCode"])
            return False

        if "선택하신 시간은 다른 분께서 예약을 하셨습니다" in html:
            self.log("선택하신 시간은 다른 분께서 예약을 하셨습니다. in step 1.2 다른 시간을 선택해 주십시요. %s %s" % (
            form_data["strReserveDate"], form_data["strReserveTime"]))
            self.add_already_booked_time(form_data["strReserveDate"], form_data["strReserveTime"],
                                         form_data["strCourseCode"])
            return False

        if '예약시간 및 개인정보를 확인 후 예약하기 버튼을 눌러주세요' in result:
            self.log("로그인 정보 오류, in step 1.3 다시 로그인 합니다.")
            self.try_login()
            return False

        url = "Reservation/ReservationCheck.aspx"
        heads = self.chinjucc_http_heads()
        heads["referer"] = "https://www.chinjucc.co.kr/Reservation/ReservationCheck.aspx"

        eventtarget = self.parser_dotnet_cache(html, "__EVENTTARGET")
        eventargument = self.parser_dotnet_cache(html, "__EVENTARGUMENT")
        viewstate = self.parser_dotnet_cache(html, "__VIEWSTATE")
        viewstategenerator = self.parser_dotnet_cache(html, "__VIEWSTATEGENERATOR")

        form_data2 = {
            "__EVENTTARGET": "ctl00$ContentPlaceHolder1$lbtOK",
            "__EVENTARGUMENT": eventargument,
            "__VIEWSTATE": viewstate,
            "__VIEWSTATEGENERATOR": viewstategenerator,
            "strReserveDate": form_data["strReserveDate"],
            "strReserveTime": form_data["strReserveTime"],  # hhmm
            "strCourseCode": form_data["strCourseCode"],  # 11 or 22...
            "strDayGubun": form_data["strDayGubun"],
            "strReserveType": form_data["strReserveType"],
            "strSeq": form_data["strSeq"],
            "strHole": form_data["strHole"],
            "strBu": form_data["strBu"],
            "dtmChangeDate": "",
            "strChangeCode": "",
            "strChangeTime": "",
            "strChangeSeq": "",
            "strChangeDayGubun": "",
            "strChangeHole": "",
            "ctl00$ContentPlaceHolder1$ddlInwon": "4",
        }

        return False
        result = self._post_with_cookie(url, data=form_data2, heads=heads)

        self.log("예약하기 step2 서버 응답결과 : status_code=%d, sz_payload=%d" % (result.status_code, len(result.text)))

        self.log_as_file(result.text, tag="step2_예약결과")
        self.log_as_file(url, tag="step2_예약_url")
        self.log_as_file(self.login_cookie, tag="step2_예약_req_login_cookie")
        self.log_as_file(heads, tag="step2_예약_heads")
        self.log_as_file(form_data2, tag="step2_예약_data")

        if "다른 회원님께서 예약 중입니다" in html:
            self.log("다른 회원님께서 예약 중입니다, in step 2.1 다른 시간을 선택해 주십시요. %s %s" % (
            form_data2["strReserveDate"], form_data2["strReserveTime"]))
            # self.add_already_booked_time(form_data["strReserveDate"], form_data["strReserveTime"],
            #                             form_data["strCourseCode"])
            return False

        if "선택하신 시간은 다른 분께서 예약을 하셨습니다" in html:
            self.log("선택하신 시간은 다른 분께서 예약을 하셨습니다. in step 2.2 다른 시간을 선택해 주십시요. %s %s" % (
            form_data2["strReserveDate"], form_data2["strReserveTime"]))
            self.add_already_booked_time(form_data["strReserveDate"], form_data["strReserveTime"],
                                         form_data["strCourseCode"])
            return False

        return True

    def chinjucc_http_heads(self):
        return {
            # ":authority": 'www.chinjucc.co.kr',
            # ":method": 'POST',
            # ":path": '/Join/Login.aspx',
            # ":scheme": 'https',
            "accept": 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-"exchange;v=b3;q=0.9',
            "accept-language": 'ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6,zh;q=0.5',
            "cache-control": 'max-age=0',
            "content-type": 'application/x-www-form-urlencoded',
            "origin": 'https://www.chinjucc.co.kr',
            "referer": 'https://www.chinjucc.co.kr/Join/Login.aspx',
            "sec-ch-ua": '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
            "sec-ch-ua-mobile": '?0',
            "sec-fetch-dest": 'document',
            "sec-fetch-mode": 'navigate',
            "sec-fetch-ccsite": 'same-origin',
            "sec-fetch-user": '?1',
            "upgrade-insecure-requests": '1',
            "user-agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36',
        }

    def make_login_data(self):
        login_url = "Join/Login.aspx"
        login_data = {
            "__EVENTTARGET": "ctl00$ContentPlaceHolder1$lbtLogin",
            "__EVENTARGUMENT": "",
            "__VIEWSTATE": "/wEPDwUJNjAyMTM0NzE1DxYCHgdQcmV2VXJsBQYvbWFpbi8WAmYPZBYCAgMPZBYGAgEPZBYIAgEPDxYCHgtOYXZpZ2F0ZVVybAUfaHR0cHM6Ly93d3cuY2hpbmp1Y2MuY28ua3IvbWFpbmRkAgMPDxYCHwEFIGh0dHBzOi8vd3d3LmVhc3RoaWxscy5jby5rci9tYWluZGQCBQ8WAh4EVGV4dAV0PGxpPjxhIGhyZWYgPSAiL0pvaW4vTG9naW4uYXNweCI+IOuhnOq3uOyduCA8L2E+PC9saT4gICA8bGk+PGEgaHJlZiA9ICIvSm9pbi9Kb2luU3RlcDEuYXNweCI+IO2ajOybkOqwgOyehSA8L2E+PC9saT5kAgcPFgIfAgWfDjxsaSBjbGFzcz0ibWVudTAxIj48YSBocmVmPSIvQWJvdXQvSW50cm9kdWN0aW9uLmFzcHgiPu2BtOufveyGjOqwnDwvYT4KICA8b2wgY2xhc3M9InNtZW51Ij4NCgkJCQkJPGxpPjxhIGhyZWY9Ii9BYm91dC9JbnRyb2R1Y3Rpb24uYXNweCI+7YG065+97IaM6rCcPC9hPjwvbGk+DQoJCQkJCTxsaT48YSBocmVmPSIvQWJvdXQvQ2VvLmFzcHgiPuyduOyCrOunkDwvYT48L2xpPg0KCQkJCQk8bGk+PGEgaHJlZj0iL0Fib3V0L0hpc3RvcnkuYXNweCI+7Jew7ZiBPC9hPjwvbGk+DQoJCQkJCTxsaT48YSBocmVmPSIvQWJvdXQvTG9jYXRpb24uYXNweCI+7Jik7Iuc64qUIOq4uDwvYT48L2xpPgogIDwvb2w+CjwvbGk+CjxsaSBjbGFzcz0ibWVudTAyIj48YSBocmVmPSIvR3VpZGUvR3VpZGUuYXNweCI+7Jq07JiB7JWI64K0PC9hPgogIDxvbCBjbGFzcz0ic21lbnUiPg0KCQkJCQk8bGk+PGEgaHJlZj0iL0d1aWRlL0d1aWRlLmFzcHgiPuydtOyaqeyViOuCtDwvYT48L2xpPg0KCQkJCQk8bGk+PGEgaHJlZj0iL0d1aWRlL0ZlZXMuYXNweCI+7J207Jqp7JqU6riIPC9hPjwvbGk+DQoJCQkJCTxsaT48YSBocmVmPSIvR3VpZGUvUmVzZXJ2ZUluZm8uYXNweCI+7JiI7JW97JWI64K0PC9hPjwvbGk+DQoJCQkJCTxsaT48YSBocmVmPSIvR3VpZGUvQ2xhdXNlLmFzcHgiPuydtOyaqeyVveq0gDwvYT48L2xpPgogIDwvb2w+CjwvbGk+CjxsaSBjbGFzcz0ibWVudTAzIj48YSBocmVmPSIvQ291cnNlL0ludHJvLmFzcHgiPuy9lOyKpOyGjOqwnDwvYT4KICA8b2wgY2xhc3M9InNtZW51Ij4NCgkJCQkJPGxpPjxhIGhyZWY9Ii9Db3Vyc2UvSW50cm8uYXNweCI+7L2U7Iqk7IaM6rCcPC9hPjwvbGk+DQoJCQkJCTxsaT48YSBocmVmPSIvQ291cnNlL0NvdXJzZURlc2lnbmVyLmFzcHgiPuy9lOyKpOqzteueteuPhDwvYT48L2xpPg0KCQkJCQk8bGk+PGEgaHJlZj0iL0NvdXJzZS9TY29yZUNhcmQuYXNweCI+7Iqk7L2U7Ja07Lm065OcPC9hPjwvbGk+DQoJCQkJCTxsaT48YSBocmVmPSIvQ291cnNlL0xvY2FsUnVsZS5hc3B4Ij7roZzsu6zro7A8L2E+PC9saT4KICA8L29sPgo8L2xpPgo8bGkgY2xhc3M9Im1lbnUwNCI+PGEgaHJlZj0iL0ZhY2lsaXRpZXMvRmFjaWxpdGllczAxLmFzcHgiPuyLnOyEpOyViOuCtDwvYT4KICA8b2wgY2xhc3M9InNtZW51Ij4NCgkJCQkJPGxpPjxhIGhyZWY9Ii9GYWNpbGl0aWVzL0ZhY2lsaXRpZXMwMS5hc3B4Ij7si5zshKTshozqsJw8L2E+PC9saT4NCgkJCQkJPGxpPjxhIGhyZWY9Ii9GYWNpbGl0aWVzL0ZhY2lsaXRpZXMwMi5hc3B4Ij7si5zshKTslYjrgrQ8L2E+PC9saT4KICA8L29sPgo8L2xpPgo8bGkgY2xhc3M9Im1lbnUwNSI+PGEgaHJlZj0iL0JvYXJkL0JvYXJkTGlzdC5hc3B4P2NmZ2lkPW5vdGljZSI+7KCV67O066eI64u5PC9hPgogIDxvbCBjbGFzcz0ic21lbnUiPg0KCQkJCQk8bGk+PGEgaHJlZj0iL0JvYXJkL0JvYXJkTGlzdC5hc3B4P2NmZ2lkPW5vdGljZSI+6rO17KeA7IKs7ZWtPC9hPjwvbGk+DQoJCQkJCTxsaT48YSBocmVmPSIvQm9hcmQvQm9hcmRMaXN0LmFzcHg/Y2ZnaWQ9ZnJlZWJvYXJkIj7qsozsi5ztjJA8L2E+PC9saT4NCgkJCQkJPGxpPjxhIGhyZWY9Ii9Cb2FyZC9HYWxsZXJ5TGlzdDIuYXNweD9jZmdpZD1jdXN0b21lcmdhbGxlcnkiPu2PrO2GoOqwpOufrOumrDwvYT48L2xpPg0KCQkJCQk8bGk+PGEgaHJlZj0iL0JvYXJkL0JvYXJkTGlzdC5hc3B4P2NmZ2lkPXBkcyI+7J6Q66OM7IukPC9hPjwvbGk+DQoJCQkJCTxsaT48YSBocmVmPSIvQm9hcmQvQm9hcmRMaXN0LmFzcHg/Y2ZnaWQ9am9pbiI+7KGw7J246rKM7Iuc7YyQPC9hPjwvbGk+CiAgPC9vbD4KPC9saT4KZAIDDxYCHwIF2wE8ZGl2IGlkPSJ2aXN1YWwiPgoJPGRpdiBjbGFzcz0idmlzdWFsX2luIj4KICAgIAk8ZGl2IGNsYXNzPSJqb2luX2JnIj4KICAgICAgICAJPHAgY2xhc3M9InZpc190dDEiPkpPSU48L3A+CiAgICAgICAgICAgIDxwIGNsYXNzPSJ2aXNfdHQyIj5UaGUgTXl0aCBPZiBHb2xmIEF0IFRoZSBDSElOSlUgQ0MgV2lsbCBCZWdpbjwvcD4KICAgICAgICA8L2Rpdj4KICAgIDwvZGl2Pgo8L2Rpdj5kAgUPFgIfAgXPAw0KCQk8bGk+PGEgaHJlZj0iL0pvaW4vTG9naW4uYXNweCI+66Gc6re47J24PC9hPjwvbGk+DQoJCTxsaT48YSBocmVmPSIvSm9pbi9Kb2luU3RlcDEuYXNweCI+7ZqM7JuQ6rCA7J6FPC9hPjwvbGk+DQoJCTxsaT48YSBocmVmPSIvSm9pbi9Bc2tJRG5QVy5hc3B4Ij7slYTsnbTrlJQv67mE67CA67KI7Zi4IOywvuq4sDwvYT48L2xpPg0KCQk8bGk+PGEgaHJlZj0iL0pvaW4vV2l0aGRyYXdhbC5hc3B4Ij7tmozsm5Dtg4jth7Q8L2E+PC9saT4NCgkJPGxpPjxhIGhyZWY9Ii9TaXRlL0FncmVlbWVudC5hc3B4Ij7snbTsmqnslb3qtIA8L2E+PC9saT4NCgkJPGxpPjxhIGhyZWY9Ii9TaXRlL1ByaXZhdGVQb2xpY3kuYXNweCI+6rCc7J247KCV67O07LKY66as67Cp7LmoPC9hPjwvbGk+DQoJCTxsaT48YSBocmVmPSIvU2l0ZS9FbWFpbFBvbGljeS5hc3B4Ij7snbTrqZTsnbzrrLTri6jsiJjsp5HqsbDrtoA8L2E+PC9saT5kZBt6sSrJo6AIGHtTbXR56YDLkDf/OewfBtSJHl+Brhq7",
            "__VIEWSTATEGENERATOR": "E3A54082",
            "__EVENTVALIDATION": "/wEdAAcKS14/RylRLbwSlo/bKQ4Vuqd4RWw5YZhOfgnuYJELYbQxWZkQxLAjkslCygpWavJ3YaFcLR3uPm6Mb620jW+eBT93GWmvjFo94Li2aAlKRXjjswiyhm+g6KodwobC/fNAA3kIsIGxY908if7afHakoylwcHCwdX6pjAgLptvs4r139X507U85jYV+1/AdfOA=",
            "ctl00$ContentPlaceHolder1$rbtLoginGroup": 160,
            "ctl00$ContentPlaceHolder1$txtUserID": self.uid,
            "ctl00$ContentPlaceHolder1$txtPassword": self.passwd,
        }
        add_headers = self.chinjucc_http_heads()
        return self._dict_url_encode(login_data), login_url, add_headers

    def try_login(self):
        self.end_session()
        self.start_session()
        if self.get_login_time_after() > 30:
            self.do_login()

    def do_login(self):
        login_data, login_url, add_headers = self.make_login_data()

        result = self._login(login_data, login_url, add_headers=add_headers)
        if result is None:
            return False
        if 'alert("비밀번호가 일치하지 않습니다.");' in result:
            self.log("비밀번호 오류로 인하여 로그인이 실패 하였습니다. : id=%s" % (self.uid,), loglv=CRITICAL)
            return False
        if self.check_login_success(result) is True:
            self.log("로그인 시도 성공 : id=%s" % (self.uid,))
            self.chinjucc_cookie_helper()
            return True
        self.login_cookie = None
        self.log("로그인 시도 실패 : id=%s" % (self.uid,))
        return False

    def check_login_success(self, html):
        if "location.href = '/main/';" in html:
            self.last_login = datetime.now()
            return True
        return False

    def init_auto_login(self):
        make_cookie_fn = self.chinjucc_http_heads
        login_data, login_url, add_headers = self.make_login_data()
        self.start_auto_login(login_data, login_url, make_cookie_fn)

    def sleep(self):
        now_hhmm = datetime.now().strftime("%H:%M")
        if now_hhmm == "08:59" or now_hhmm == "09:00":
            time.sleep(0.125)
            return
        time.sleep(self.sleep_time)
        return
