import json
from typing import List

class GeojeviewccCourseListNode:
    yyyymmdd : str
    bk_cours: str
    bk_part: str
    bk_time: str
    bk_hole: int
    bk_base_greenfee: str
    bk_green_fee: int
    bk_event: str
    bk_green_sale_rate: str
    bk_cours_name: str

    def __init__(self, yyyymmdd: str,
                 bk_cours: str,
                 bk_part: str,
                 bk_time: str,
                 bk_hole: str,
                 bk_base_greenfee: str,
                 bk_green_fee: str,
                 bk_event: str,
                 bk_green_sale_rate: str,
                 bk_cours_name: str) -> None:
        super().__init__()
        self.bk_cours = bk_cours
        self.bk_part = bk_part
        self.bk_time = bk_time
        self.bk_hole = int(bk_hole)
        self.bk_base_greenfee = bk_base_greenfee
        self.bk_green_fee = int(bk_green_fee.replace(",", ""))
        self.bk_event = bk_event
        self.bk_green_sale_rate = bk_green_sale_rate
        self.bk_cours_name = bk_cours_name
        self.yyyymmdd = yyyymmdd

class GeojeviewccCourseListModel:
    course_list : List[GeojeviewccCourseListNode]
    def __init__(self, html, yyyymmdd) -> None:
        super().__init__()
        self.course_list = []
        if len(html) == 0:
            return
        json_data: list = json.loads(html)
        for item in json_data:
            self.course_list.append(GeojeviewccCourseListNode(
                yyyymmdd,
                item["bk_cours"],
                item["bk_part"],
                item["bk_time"],
                item["bk_hole"],
                item["bk_base_greenfee"],
                item["bk_green_fee"],
                item["bk_event"],
                item["bk_green_sale_rate"],
                item["bk_cours_name"],
            ))


'''
[
    {
        "bk_cours":"A",
        "bk_part":"1",
        "bk_time":"0623",
        "bk_hole":"18",
        "bk_base_greenfee":"-",
        "bk_green_fee":"125,000",
        "bk_event":"",
        "bk_green_sale_rate":"-",
        "bk_cours_name":"\ud574\ub3cb\uc774"
    },
    {
        "bk_cours":"A",
        "bk_part":"3",
        "bk_time":"1919",
        "bk_hole":"18",
        "bk_base_greenfee":"-",
        "bk_green_fee":"110,000",
        "bk_event":"",
        "bk_green_sale_rate":"-",
        "bk_cours_name":"\ud574\ub3cb\uc774"
    },
    {
        "bk_cours":"B",
        "bk_part":"3",
        "bk_time":"1919",
        "bk_hole":"18",
        "bk_base_greenfee":"-",
        "bk_green_fee":"110,000",
        "bk_event":"",
        "bk_green_sale_rate":"-",
        "bk_cours_name":"\ud574\ub118\uc774"
    },
    {
        "bk_cours":"A",
        "bk_part":"3",
        "bk_time":"1926",
        "bk_hole":"18",
        "bk_base_greenfee":"-",
        "bk_green_fee":"110,000",
        "bk_event":"",
        "bk_green_sale_rate":"-",
        "bk_cours_name":"\ud574\ub3cb\uc774"
    }
]
'''