import urllib

SITE_ADDR = "https://www.geojeview.co.kr"


class GeojeviewccHttp:
    def __init__(self) -> None:
        super().__init__()

    @staticmethod
    def make_url(login_url) -> str:
        if login_url[0] == '/':
            login_url = login_url[1:]
        return "%s/%s" % (SITE_ADDR, login_url)

    @staticmethod
    def http_heads():
        return {
            # ":authority": 'www.chinjucc.co.kr',
            # ":method": 'POST',
            # ":path": '/Join/Login.aspx',
            # ":scheme": 'https',
            "accept": 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-"exchange;v=b3;q=0.9',
            "accept-language": 'ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6,zh;q=0.5',
            "cache-control": 'max-age=0',
            "content-type": 'application/x-www-form-urlencoded',
            "origin": SITE_ADDR,
            "sec-ch-ua": '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
            "sec-ch-ua-mobile": '?0',
            "sec-fetch-dest": 'document',
            "sec-fetch-mode": 'navigate',
            "sec-fetch-ccsite": 'same-origin',
            "sec-fetch-user": '?1',
            "upgrade-insecure-requests": '1',
            "user-agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36',
        }