import concurrent.futures
import time
from typing import List

import requests

from ccsite.geojeviewcc.http_helper import GeojeviewccHttp
from ccsite.geojeviewcc.models.course_list_model import GeojeviewccCourseListNode
from chinjucc.http_helper import ChinjuccHttp
from chinjucc.models.course_list_model import ChinjuccCourseListModel
from common.common_http_helper import CommonHttpHelper
from common.order_model import OrderReservation
from chinjucc.parser_header import ChinjuccParserHeader

from common.cc_logging import Logging


class GeojeviewccBook:
    is_book_success: bool
    old_booking_count: int

    def __init__(self) -> None:
        super().__init__()
        self.is_book_success = False
        self.old_booking_count = 0

    def do_book_multi(self, course_list: List[GeojeviewccCourseListNode], order: OrderReservation,
                      session: requests.sessions.Session) -> bool:
        idx_ok = None
        thread_list = []
        time.sleep(1.0)
        token: str = self.request_token(session)
        for idx, course in enumerate(course_list):
            if self.is_book_success is True:
                break
            executor = concurrent.futures.ThreadPoolExecutor()
            h_thread = executor.submit(self.do_book, course, order, session, token)
            thread_list.append(h_thread)
            time.sleep(0.25)
        for idx, h_thread in enumerate(thread_list):
            if h_thread.result() is True:
                idx_ok = idx
        if idx_ok is None:
            return False
        return True

    def request_token(self, session: requests.sessions.Session) -> str:
        result = session.get(GeojeviewccHttp.make_url("/booking"))
        token = result.text.split('<meta name="csrf-token" content="')[1].split('"')[0]
        return token

    def create_order(self, course: GeojeviewccCourseListNode, order: OrderReservation,
                     session: requests.sessions.Session):
        create_booking = GeojeviewccHttp.make_url("/booking/create?date=%s&course=%s&time=%s&hole=%d" % (
        course.yyyymmdd, course.bk_cours, course.bk_time.replace(":", ""), course.bk_hole))
        result = session.get(create_booking)
        return

    def do_book(self,
                course: GeojeviewccCourseListNode,
                order: OrderReservation,
                session: requests.sessions.Session,
                token: str) -> bool:
        if self.is_book_success is True:
            return True
        self.create_order(course, order, session)
        book_url = GeojeviewccHttp.make_url("/booking")
        form_data = self.make_reservation_model(token, course)
        headers = GeojeviewccHttp.http_heads()
        if self.is_book_success is True:
            return True
        result = session.post(book_url, data=form_data, headers=headers)
        #return self.check_result(result.text, form_data, result.status_code)
        return self.__check_result_by_page(session)

    def make_reservation_model(self, token: str, course: GeojeviewccCourseListNode) -> str:
        return CommonHttpHelper.dict_url_encode({
            "_token": token,
            "date": course.yyyymmdd,
            "cours": course.bk_cours,
            "time": course.bk_time.replace(":", ""),
            "hole": course.bk_hole,
            "incnt": 4,
            "booking_agree": 0,
        }) + "&booking_agree=1"

    def check_result(self, html: str, form_data: str, status_code: int) -> bool:
        if status_code == 200:
            self.is_book_success = True
            return True
        return False

    def __get_booking_count(self, session: requests.sessions.Session):
        result = session.get(GeojeviewccHttp.make_url("/booking/history"), headers=GeojeviewccHttp.http_heads())
        return result.text.count("btn btn-sm btn-default btn-booking-change-time")

    def __check_result_by_page(self, session: requests.sessions.Session):
        new_booking_count = self.__get_booking_count(session)
        if self.old_booking_count < new_booking_count:
            self.old_booking_count = new_booking_count
            return True
        return False
