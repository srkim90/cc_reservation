import time
from datetime import datetime, timedelta
from typing import List

from common.cc_logging import Logging


class GeojeviewccTimeChecker:
    def __init__(self) -> None:
        super().__init__()

    def get_auth_login_ranges(self) -> List[List[str]]:
        return [
            ["09:55", "10:15"],
        ]

    def check_is_reservation_time_available(self, yyyymmdd):
        s_hhmmss = self.get_auth_login_ranges()[0][0].replace(":", "") + "00"
        target_day: datetime = datetime.strptime("%s %s" % (yyyymmdd, s_hhmmss), "%Y%m%d %H%M%S")
        possible_time = target_day - timedelta(days=(14 + target_day.weekday()))
        now_time = datetime.now()
        if possible_time < now_time:
            return True
        time_gab = possible_time - now_time
        day = time_gab.days
        hh = int(time_gab.seconds / 3600)
        mm = int((time_gab.seconds % 3600) / 60)
        ss = time_gab.seconds % 60

        if day == 0:
            n_wait = 60
            info_str = "[대기중] 예약가능시간:%s, 남은시간:(%d시 %02d분 %02d초)" % (possible_time, hh, mm, ss)
        else:
            n_wait = 600
            info_str = "[대기중] 예약가능시간:%s, 남은시간:(%d일 %02d시 %02d분 %02d초)" % (possible_time, day, hh, mm, ss)

        Logging.log(info_str)
        time.sleep(n_wait)
        return False
