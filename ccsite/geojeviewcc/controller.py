import datetime
import time

import requests

from ccsite.geojeviewcc.book import GeojeviewccBook
from ccsite.geojeviewcc.course_list import GeojeviewccCourseList
from ccsite.geojeviewcc.login import GeojeviewccLogin
from ccsite.geojeviewcc.time_checker import GeojeviewccTimeChecker
from common.cc_logging import Logging
from common.order_model import OrderReservation
from define import ROBOT_VERSION, UPDATE_DATE, AUTHOR
from dialog import logWindow


class GeojeviewccController:
    order: OrderReservation
    course_list: GeojeviewccCourseList
    login: GeojeviewccLogin
    session: requests.sessions.Session
    book: GeojeviewccBook
    last_login: datetime.datetime
    time_checker: GeojeviewccTimeChecker

    def re_order(self):
        self.login = GeojeviewccLogin(self.order)
        self.course_list = GeojeviewccCourseList()
        self.book = GeojeviewccBook()
        self.last_login = None
        self.time_checker = GeojeviewccTimeChecker()

    def __init__(self, order: OrderReservation) -> None:
        super().__init__()
        self.order = order
        self.re_order()

        self.site_name = "거제뷰CC"
        self.site_addr = "https://www.geojeview.co.kr/"

    def _need_login(self, yyyymmdd) -> bool:
        if self.last_login is None:
            return True
        timediff = datetime.datetime.now() - self.last_login
        if timediff.seconds > 600:
            return True
        return False

    def run(self) -> bool:
        yyyymmdds = self.order.yyyymmdd.replace(" ","").split(",")
        time.sleep(1)
        for idx,yyyymmdd in enumerate(yyyymmdds):
            self.order.yyyymmdd = yyyymmdd
            while True:
                if self._need_login(self.order.yyyymmdd) is True:
                    token, self.session = self.login.do_login()
                    self.last_login = datetime.datetime.now()
                if self.time_checker.check_is_reservation_time_available(self.order.yyyymmdd) is False:
                    continue
                try:
                    course_list = self.course_list.get_list(self.order, self.session)
                    result = self.book.do_book_multi(
                        course_list,
                        self.order,
                        self.session)
                    if result is True:
                        if idx + 1 == len(yyyymmdds):
                            Logging.log("예약 성공, 프로그램 종료 합니다")
                            return True
                        else:
                            Logging.log("%s : 예약 성공, 다음 날짜로 넘어갑니다. (%d/%d)" % (yyyymmdd, idx+1, len(yyyymmdds)))
                            time.sleep(1.5)
                            self.re_order()
                            break
                except Exception as e:
                    continue
                time.sleep(5)

    def log_window_th(self, app):
        self.log_window = logWindow()
        Logging.set_log_window(self.log_window)
        self.log_window.resize(640, 480)
        self.log_window.show()
        self.print_init_log()
        app.exec_()

    def get_course_mapping(self, course_info):
        return GeojeviewccCourseList.get_course_mapping(course_info)


    def print_init_log(self):
        Logging.log("%s<%s> 자동 예약 프로그램 기동 성공" % (self.site_name, self.site_addr))
        Logging.log("version=%s date=%s, author=%s" % (ROBOT_VERSION, UPDATE_DATE, AUTHOR))
        Logging.log("  -- 사이트 이름 : %s <%s>" % (self.site_name, self.site_addr))
        Logging.log("  -- 가입자 계정 : %s" % (self.order.uid,))
        Logging.log("  -- 가입자 비밀번호 : %s" % ("*" * len(self.order.passwd),))
        for yyyymmdd in self.order.yyyymmdd.replace(" ","").split(","):
            Logging.log("  -- 예약 일자 : %s (%s)" % (yyyymmdd, self.get_weekday(yyyymmdd)))
        Logging.log("  -- 예약 시간 : %s" % (self.order.get_prefer_time(),))

    def get_weekday(self, yyyymmdd):
        week_name = ['월', '화', '수', '목', '금', '토', '일']
        weekday = datetime.datetime.strptime("%s 12:00:00" % yyyymmdd, "%Y%m%d %H:%M:%S").weekday()
        return week_name[weekday]
