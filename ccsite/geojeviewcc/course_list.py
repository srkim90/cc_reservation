import time
from datetime import datetime, timedelta
from operator import itemgetter
from typing import List, Tuple

import requests

from ccsite.geojeviewcc.http_helper import GeojeviewccHttp
from ccsite.geojeviewcc.models.course_list_model import GeojeviewccCourseListModel, GeojeviewccCourseListNode
from ccsite.geojeviewcc.time_checker import GeojeviewccTimeChecker
from common.order_model import OrderReservation
from common.cc_logging import Logging


class GeojeviewccCourseList:
    http_helper: GeojeviewccHttp


    def __init__(self) -> None:
        super().__init__()
        self.http_helper = GeojeviewccHttp()


    def get_list(self,
                 order: OrderReservation,
                 session: requests.sessions.Session) -> List[GeojeviewccCourseListNode]:

        # if self.time_checker.check_is_reservation_time_available(order.yyyymmdd) is False:
        #     raise ModuleNotFoundError

        result = session.get(GeojeviewccHttp.make_url("/booking/time/%s" % order.yyyymmdd))

        origin_list: List[GeojeviewccCourseListNode] = GeojeviewccCourseListModel(result.text, order.yyyymmdd).course_list

        return self.select_time(origin_list, order.yyyymmdd, order.get_prefer_time())

    def select_time(self, course_list: List[GeojeviewccCourseListNode], yyyymmdd, prefer_time, n_result=1) -> List[GeojeviewccCourseListNode]:
        if len(course_list) == 0:
            return None
        prefer_date = datetime.strptime("%s%s00" % (yyyymmdd, prefer_time.replace(":", "")),
                                        "%Y%m%d%H%M%S").timestamp()
        time_gap = 3600 * 128  # 의미없는 큰 시간
        select_at: List[GeojeviewccCourseListNode]
        Logging.log("=========================================")
        Logging.log("예약 가능한 시간대 리스트")
        Logging.log("-----------------------------------------")
        all_list = []
        for idx, item in enumerate(course_list):
            add_info = ""
            Logging.log("  - %s %s %s(%s) %s" % (
                item.yyyymmdd, item.bk_time, GeojeviewccCourseList.get_course_mapping(item.bk_cours), item.bk_cours, add_info))
            # if self.check_already_booked_time(item["yyyymmdd"], item["hhmm"], item["course"]) is True:
            #     Logging.log("SKIP")
            #     continue
            date_at = datetime.strptime("%s%s00" % (item.yyyymmdd, item.bk_time), "%Y%m%d%H%M%S").timestamp()
            time_gap_at = abs(prefer_date - date_at)
            if time_gap > time_gap_at:
                time_gap = time_gap_at
            all_list.append({
                "time_gap": time_gap_at,
                "item": item
            })
        Logging.log("-----------------------------------------")
        Logging.log("선택 된 시간")

        _all_list = sorted(all_list, key=itemgetter('time_gap'), reverse=False)
        all_list = []
        for item in _all_list:
            all_list.append(item["item"])
        if len(all_list) >= n_result:
            select_at = all_list[0:n_result]
        else:
            select_at = all_list
        for item in select_at:
            Logging.log("  - %s %s %s" % (item.yyyymmdd, item.bk_time, GeojeviewccCourseList.get_course_mapping(item.bk_cours)))
        Logging.log("=========================================")
        return select_at

    @staticmethod
    def get_course_mapping(course_info):
        if course_info == None:
            return "임의의코스"
        course_dict = {
            "임의의코스": 0,
            "해돋이": "A",
            "해넘이": "B",
        }
        # 이름으로 들어올 경우 숫자값 반환
        if course_info in course_dict.keys():
            return course_dict[course_info]

        if type(course_info) == str:
            try:
                course_info = int(course_info)
            except:
                pass

        # 숫자값으로 들어올 경우 이름 반환
        for course_name in course_dict.keys():
            if course_dict[course_name] == course_info:
                return course_name

        return None
