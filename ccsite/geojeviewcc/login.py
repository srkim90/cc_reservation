import urllib
from datetime import datetime
from http import HTTPStatus



from typing import Tuple

import requests

from ccsite.geojeviewcc.http_helper import GeojeviewccHttp
from ccsite.geojeviewcc.time_checker import GeojeviewccTimeChecker
from chinjucc.http_helper import ChinjuccHttp
from common.order_model import OrderReservation
from common.cc_logging import Logging
from common.cc_loglevel import *


class GeojeviewccLogin:
    http_helper : ChinjuccHttp
    uid: str
    passwd: str
    last_login : datetime
    token: str
    time_checker: GeojeviewccTimeChecker

    def __init__(self, order: OrderReservation) -> None:
        super().__init__()
        self.uid = order.uid
        self.passwd = order.passwd
        self.session = requests.sessions.Session()
        self.last_login = None
        self.token = None
        self.time_checker = GeojeviewccTimeChecker()

    def check_login_success(self, html):
        if "로그아웃" in html:
            self.last_login = datetime.now()
            return True
        return False

    def request_token(self):
        result = self.session.get(GeojeviewccHttp.make_url("/auth/login"))
        self.token = result.text.split('<input type="hidden" name="_token" value="')[1].split('"')[0]
        return self.token

    def make_login_data(self):
        login_data = {
            "_token": self.request_token(),
            "username": self.uid,
            "password": self.passwd,
            "remember": 1
        }
        login_url = GeojeviewccHttp.make_url("/auth/login")

        return urllib.parse.urlencode(login_data), login_url

    def do_login(self) -> Tuple[str, requests.sessions.Session]:
        self.session = requests.sessions.Session()
        login_data, login_url = self.make_login_data()

        result = self.session.post(login_url, data=login_data, headers=GeojeviewccHttp.http_heads())
        text = result.text
        if result is None:
            raise Exception
        if 'alert("비밀번호가 일치하지 않습니다.");' in text:
            Logging.log("비밀번호 오류로 인하여 로그인이 실패 하였습니다. : id=%s" % (self.uid,), loglv=CRITICAL)
            raise Exception
        if self.check_login_success(text) is True:
            Logging.log("로그인 시도 성공 : id=%s" % (self.uid,))
        return self.token, self.session