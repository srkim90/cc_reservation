import copy
from datetime import datetime
import inspect
import json

def time_to_string_dict(dictAt):
    dictAt = copy.deepcopy(dictAt)
    for key_name in dictAt.keys():
        if type(dictAt[key_name]) == dict:
            dictAt[key_name] = time_to_string_dict(dictAt[key_name])
        elif type(dictAt[key_name]) == list:
            for idx,item in enumerate(dictAt[key_name]):
                #print(type(dictAt[key_name][idx]))
                if type(dictAt[key_name][idx]) == datetime:
                    #print("SAAA")
                    dictAt[key_name][idx] = dictAt[key_name][idx].strftime("%Y-%m-%d %H:%M:%S")
        elif type(dictAt[key_name]) == datetime:
            dictAt[key_name] = dictAt[key_name].strftime("%Y-%m-%d %H:%M:%S")
    return dictAt


def printDict(dictAt, cout=True):
    if type(dictAt) != dict:
        print("%s" % dictAt)
        return None
    try:
        jsonAt = json.dumps(dictAt, indent=4)
    except TypeError as e:
        try:
            dictAt = time_to_string_dict(dictAt)
        except Exception:
            print("%s" % dictAt)
            return None
        try:
            jsonAt = json.dumps(dictAt, indent=4)
        except TypeError:
            print("%s" % dictAt)
            return None

    if cout is True:
        print(jsonAt)
    return jsonAt


def func(n_caller_stack=1):
    caller = inspect.stack()[n_caller_stack]
    return "%s(%d):%s" % (caller.filename, caller.lineno, caller.function)
