import json
import threading
from datetime import datetime, timedelta
from http import HTTPStatus
from time import *
from logging import *
import urllib

from dialog import logWindow
import requests
from define import *

class shadowBotBaseNobelCc:
    def __init__(self, site_name, site_addr, user_name, user_phone, yyyymmdd, prefer_time, uid, passwd, prefer_course_idx=None):
        self.site_name = site_name
        self.site_addr = site_addr
        self.login_cookie = None
        self.login_time = None
        self.user_name = user_name
        self.user_phone = user_phone
        self.prefer_course_idx = prefer_course_idx
        self.prefer_time = prefer_time
        self.yyyymmdd = [yyyymmdd,]
        self.login_timeout_sec = None
        self.uid = uid
        self.passwd = passwd
        self.auth_login_ranges = None # [["08:40", "09:20"], ["14:40", "15:20"]]
        self.log_window = None
        self.ln_try = 0
        self.last_report = None
        self.sleep_time = 1.0
        self.report_delay = 300
        self.weekday = self.get_weekday(yyyymmdd)
        self.session = None

    def start_session(self):
        self.session = requests.sessions.Session()

    def end_session(self):
        if self.session is not None:
            self.session.close()
            self.session = None


    def get_user_info(self):
        pass

    def check_cancel(self):
        return False

    def get_weekday(self, yyyymmdd):
        week_name = ['월', '화', '수', '목', '금', '토', '일']
        weekday = datetime.strptime("%s 12:00:00" % yyyymmdd, "%Y%m%d %H:%M:%S").weekday()
        return week_name[weekday]

    def add_try_history(self):
        self.ln_try += 1
        f_report = False
        if self.last_report == None:
            f_report = True
        else:
            if datetime.now().timestamp() - self.last_report.timestamp() > self.report_delay:
                f_report = True
        if f_report == False:
            return None

        from_hhmmss = datetime.now().strftime("%H:%M:%S")
        if self.last_report is not None:
            from_hhmmss = self.last_report.strftime("%H:%M:%S")
        end_hhmmss = datetime.now().strftime("%H:%M:%S")
        log_string = "%s ~ %s 동안 %d 회 예약 가능 여부 확인 시도 하였습니다." % (from_hhmmss, end_hhmmss, self.ln_try)
        if self.last_report == None:
            log_string = [log_string,]
            log_string.append("이후 %s초마다 예약 시도하며, %s초마다 보고 합니다." % (self.sleep_time, self.report_delay))

        self.last_report = datetime.now()
        self.ln_try = 0
        return log_string


    def print_init_log(self):
        self.log("%s<%s> 자동 예약 프로그램 기동 성공" % (self.site_name, self.site_addr))
        self.log("version=%s date=%s, author=%s" % (ROBOT_VERSION, UPDATE_DATE, AUTHOR))
        self.log("  -- 사이트 이름 : %s <%s>" % (self.site_name, self.site_addr))
        self.log("  -- 가입자 계정 : %s" % (self.uid,))
        self.log("  -- 가입자 비밀번호 : %s" % ("*" * len(self.passwd),))
        for yyyymmdd in self.yyyymmdd:
            self.log("  -- 예약 일자 : %s (%s)" % (yyyymmdd, self.get_weekday(yyyymmdd)))
        self.log("  -- 예약 시간 : %s" % (self.prefer_time,))
        self.log("  -- 예약 코스 : %s" % (self.get_course_mapping(self.prefer_course_idx),))


    def log_window_th(self, app):
        self.log_window = logWindow()
        self.log_window.resize(640, 480)
        self.log_window.show()
        self.print_init_log()
        app.exec_()

    def log(self, log_text, loglv=DEBUG):
        #log_text = "%s" % log_text
        if self.log_window == None:
            print(log_text)
        else:
            self.log_window.log_fn(log_text, loglv)

    def sleep(self):
        sleep(self.sleep_time)

    def _make_url(self, login_url):
        return "%s/%s" % (self.site_addr, login_url)

    def _dict_url_encode(self, data, order=None):
        # result = ""
        # if order == None:
        #     order = data.keys()
        # for idx,key in enumerate(order):
        #     if idx != 0:
        #         result += "&"
        #     result += "%s=%s" % (key, data[key])
        # result = "\r\n" + result

        return urllib.parse.urlencode(data)

    def html_header_to_json(self, html_head):
        new_dict = {}
        for item in html_head.keys():
            new_dict[item] = "%s" % html_head[item]
        return json.dumps(new_dict, indent=4)

    def add_cookies(self, cookie_list):
        result_cookie = ""
        ignore = ["expires", "max-age", "samesite",] # "path",
        already_exist = []
        for str_cookie in cookie_list:
            if str_cookie == None:
                continue
            for item in str_cookie.split(";"):
                item = item.strip()
                if '=' not in item:
                    continue
                tag = item.split("=")[0].split(" ")[-1]
                val = item.split("=")[1]

                if tag.lower() in ignore:
                    continue
                if tag in already_exist:
                    continue
                if len(result_cookie) != 0:
                    result_cookie += "; "
                result_cookie += "%s=%s" % (tag, val)
                already_exist.append(tag)
        return result_cookie

    def display_cookies(self):
        login_cookie:str = self.login_cookie
        login_cookie = login_cookie.split(";")
        print("--------------------------")
        for cookie in login_cookie:
            cookie = cookie.strip()
            key = cookie.split("=")[0]
            value = cookie.split("=")[-1]
            print("key : %s, value : %s" % (key, value))
        print("--------------------------")


    def _login(self, login_data, login_url, add_headers=None):
        login_url = self._make_url(login_url)

        headers = self._make_http_heades()
        if type(login_data) == str:
            headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
            headers['Host'] = "www.geojeview.co.kr"
            headers['Origin'] = "https://www.geojeview.co.kr"
            headers['Referer'] = "https://www.geojeview.co.kr/auth/login"
            headers['sec-ch-ua'] = 'Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"'
            headers['sec-ch-ua-mobile'] = "?0"
            headers['Sec-Fetch-Dest']  = "empty"
            headers['Sec-Fetch-Mode'] = "cors"
            headers['Sec-Fetch-Site'] = "same-origin"
            headers['X-Requested-With'] = "XMLHttpRequest"

        org_cookie = None
        if add_headers is not None:
            for key_name in add_headers.keys():
                headers[key_name] = add_headers[key_name]
                if key_name == "Cookie":
                    org_cookie = add_headers[key_name]

        if self.session is not None:
            if "Cookie" in headers.keys():
                del headers["Cookie"]
            res = self.session.post(login_url, data=login_data, headers=headers)
        else:
            res = requests.post(login_url, data=login_data, headers=headers)
        res.encoding = "UTF-8"

        #print("response : %s\n%s" % (res.status_code, self.html_header_to_json(res.headers)))
        #print("request  : %s" % self.html_header_to_json(res.request.headers))
        #print(res.text)
        #sleep(999)
        if res.status_code == HTTPStatus.OK or res.status_code == HTTPStatus.ACCEPTED:
            if "Set-Cookie" not in res.headers.keys():
                return None
            self.login_cookie = res.headers["Set-Cookie"] #self.add_cookies([res.headers["Set-Cookie"], org_cookie])
            self.login_time = datetime.now();
            #self.log(self.login_cookie)
            #print(self.login_cookie)
            #sleep(9999)
            result = res.text  # .encode("utf-8").decode("utf-8")
            return result
        else:
            return None

    def _logout(self):
        self.login_cookie = None

    def need_login(self):
        if self.login_time is None:
            return True
        time_gap = datetime.now().timestamp() - self.login_time.timestamp()
        if time_gap > self.login_timeout_sec:
            return True
        return False

    def start_auto_login(self, login_data, login_url, make_cookie_fn=None):
        hThread = threading.Thread(target=self._login_thread, args=(login_data, login_url, make_cookie_fn))
        hThread.daemon = True
        hThread.start()

    def _login_thread(self, login_data, login_url, make_cookie_fn):
        sleep(2)
        cookie = None
        while True:
            if self.auth_login_ranges is not None:
                f_skip = True
                for range_pair in self.auth_login_ranges:
                    today_yyyymmdd = "%d%02d%02d" % (datetime.now().year, datetime.now().month, datetime.now().day)
                    start_time = datetime.strptime("%s%s00" % (today_yyyymmdd, range_pair[0].replace(":", "")), "%Y%m%d%H%M%S")
                    end_time = datetime.strptime("%s%s00" % (today_yyyymmdd, range_pair[1].replace(":", "")), "%Y%m%d%H%M%S")
                    now_time = datetime.now()
                    #print("start_time : %s" % start_time)
                    #print("end_time : %s" % end_time)
                    #print("now_time : %s" % now_time)
                    if start_time < now_time < end_time:
                        f_skip = False
                        break
            else:
                f_skip = False
            if self.need_login() and f_skip is False:
                #self.log("자동 로그인 동작 : id=%s, 동작시간 : %s" % (self.uid, self.auth_login_ranges))
                self._logout()
                if make_cookie_fn is not None:
                    cookie = make_cookie_fn()
                result =  self._login(login_data, login_url, cookie)
                if "location.replace('/index.asp');" in result:
                    self.log("자동 로그인 동작 성공 : id=%s" % (self.uid, ))
                else:
                    self.log("자동 로그인 동작, 로그인 실패!!! : id=%s, login_data=%s, login_url=%s" % (self.uid, login_data, login_url))
                sleep(60.0)
            else:
                sleep(1.0)

    def _make_http_heades(self):
        headers = {
            'User-Agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36",
            'Accept-Language': "ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6,zh;q=0.5",
        }
        return headers

    def _post_with_cookie(self, url, data):
        url = self._make_url(url)

        #cookies = {'Cookie': self.login_cookie}
        headers = self._make_http_heades()
        #cookies['Cookie'] = self.login_cookie
        if self.session is not None:
            result = self.session.post(url, data=data, headers=headers)
        else:
            cookies = {'Cookie': self.login_cookie}
            result = requests.post(url, data=data, headers=headers, cookies=cookies)
        result.encoding = 'utf-8'

        # print(url)
        # print("response : %s\n%s" % (result.status_code, self.html_header_to_json(result.headers)))
        # print("request  : %s" % self.html_header_to_json(result.request.headers))
        # print(data)
        #print(result.text)

        return result

    def _post_text_with_cookie(self, url, data):
        result = self._post_with_cookie(url, data)
        return result.text

    def _get_with_cookie(self, url, params=None):
        url = self._make_url(url)
        #cookies = {'Cookie': self.login_cookie}
        headers = self._make_http_heades()
        #cookies['Cookie'] = self.login_cookie
        if self.session is not None:
            result = self.session.get(url, params=params, headers=headers)
        else:
            cookies = {'Cookie': self.login_cookie}
            result = requests.get(url, params=params, headers=headers, cookies=cookies)
        result.encoding = 'utf-8'

        print(url)
        print("response : %s\n%s" % (result.status_code, self.html_header_to_json(result.headers)))
        print("request  : %s" % self.html_header_to_json(result.request.headers))

        return result

    def _get_text_with_cookie(self, url, params=None):
        result = self._get_with_cookie(url, params)
        return result.text

    def get_course_mapping(self, course_info):
        return ""

    def select_time(self, list_data, yyyymmdd):
        prefer_date = datetime.strptime("%s%s00" % (yyyymmdd, self.prefer_time.replace(":", "")),
                                        "%Y%m%d%H%M%S").timestamp()
        time_gap = 3600 * 128 # 의미없는 큰 시간
        select_at = None
        self.log("=========================================")
        self.log("예약 가능한 시간대 리스트")
        self.log("-----------------------------------------")
        for idx, item in enumerate(list_data):
            self.log("  - %s %s %s" % (item["yyyymmdd"], item["hhmm"], self.get_course_mapping(item["course"])))
            date_at = datetime.strptime("%s%s00" % (item["yyyymmdd"], item["hhmm"]), "%Y%m%d%H%M%S").timestamp()
            if time_gap > abs(prefer_date - date_at):
                time_gap = abs(prefer_date - date_at)
                select_at = item
        self.log("-----------------------------------------")
        self.log("선택 된 시간")
        self.log("  - %s %s %s" % (select_at["yyyymmdd"], select_at["hhmm"], self.get_course_mapping(select_at["course"])))
        self.log("=========================================")

        return select_at

